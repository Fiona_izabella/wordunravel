/*validate contact-us form*/
$(document).bind("pageshow", "#registerPage", function() {


    $.validator.addMethod("passmatch", function(value) {
            return value == $("#password").val();
    }, 'Confirmation password must match.');

    $("#registerForm").validate({

        errorPlacement: function(error, element) {
            if (element.attr("name") === "favcolor") {
                error.insertAfter($("#favcolor").parent());
            } else {
                error.insertAfter(element);
            }
        }

    });


/*draggabe widget on platform page*/

$(function() {
   $('#drag1').draggable();
  });
    

});

/** 
audio buttons 
**/

$( "#verify" ).on( "vmousedown", function( event ) {  
  //alert("vmousedown verify");
  console.log('Button v has been clicked');

  var text = document.getElementById("testToSpeech").innerHTML;
//console.log(text);
 
if (oggSupported()) {
  document.getElementById("audioholder").innerHTML = "";

  var audioElement = document.createElement('audio');
  audioElement.setAttribute('src', "http://speechutil.com/convert/ogg?text='" + escape(text) + "'");
  audioElement.play();
  document.getElementById("audioholder").appendChild(audioElement);
} else {
  document.getElementById("audioholder").innerHTML = "<EMBED src=\"http://speechutil.com/convert/wav?text='" + escape(text) + "'\" autostart=true loop=false volume=100 HIDDEN=\"true\"/>";
}


});


$( "#verify2" ).on( "vmousedown", function( event ) {  
     console.log('Button v2 has been clicked');
    //alert("vmousedown verify2");

  var text2 = document.getElementById("testToSpeech2").innerHTML;
// var text = "hello miss lisa ";
//console.log('text');
// alert(text);
if (oggSupported()) {
  document.getElementById("audioholder2").innerHTML = "";

  var audioElement2 = document.createElement('audio2');
  audioElement2.setAttribute('src', "http://speechutil.com/convert/ogg?text='" + escape(text2) + "'");
  audioElement2.play();
  document.getElementById("audioholder2").appendChild(audioElement);
} else {
  document.getElementById("audioholder2").innerHTML = "<EMBED src=\"http://speechutil.com/convert/wav?text='" + escape(text2) + "'\" autostart=true loop=false volume=100 HIDDEN=\"true\"/>";
}


});

$( "#verify3" ).on( "vmousedown", function( event ) {  
   console.log('Button v3 has been clicked');
    //alert("vmousedown verify3");

  var text3 = document.getElementById("testToSpeech3").innerHTML;
// var text = "hello miss lisa ";
//console.log('text');
// alert(text);
if (oggSupported()) {
  document.getElementById("audioholder3").innerHTML = "";

  var audioElement3 = document.createElement('audio3');
  audioElement3.setAttribute('src', "http://speechutil.com/convert/ogg?text='" + escape(text3) + "'");
  audioElement3.play();
  document.getElementById("audioholder3").appendChild(audioElement);
} else {
  document.getElementById("audioholder3").innerHTML = "<EMBED src=\"http://speechutil.com/convert/wav?text='" + escape(text3) + "'\" autostart=true loop=false volume=100 HIDDEN=\"true\"/>";
}


});

/*internel pages do not include jquerymobile script due to conflict between jquery
mobile and codeigniter form submissions so am using bind touchsmart method instead for audio*/
//ose bind or on/ I recently changed on to bind
$('#verify4').on('touchstart click', function(e){
  //alert('test4');
 console.log('Button v4 has been clicked');

    e.stopPropagation(); e.preventDefault();
    

var text = document.getElementById("testToSpeech").innerHTML;
   
if (oggSupported()) {
  document.getElementById("audioholder").innerHTML = "";
  var audioElement = document.createElement('audio');
  audioElement.setAttribute('src', "http://speechutil.com/convert/ogg?text='" + escape(text) + "'");
  audioElement.play();
  document.getElementById("audioholder").appendChild(audioElement);
} else {
  document.getElementById("audioholder").innerHTML = "<EMBED src=\"http://speechutil.com/convert/wav?text='" + escape(text) + "'\" autostart=true loop=false volume=100 HIDDEN=\"true\"/>";
}


});


function oggSupported() {
  var a = document.createElement('audio');
  if (a != null) {
    var agt = navigator.userAgent.toLowerCase();
    if (agt.indexOf("chrome") != -1) {
      return 0;
    }
    if (agt.indexOf("safari") != -1) {
      return 0;
    }

    if (a.canPlayType != null) {
      if (a.canPlayType("audio/ogg") != "no")
        return 1;
    }
  }
  return 0;
}

/** 
end of audio buttons 
**/

/*slides functionality*/

  $("#listitem").swipeleft(function() {
    $.mobile.changePage("#page2");
  });
  $("#listitem2").swipeleft(function() {
    $.mobile.changePage("#page3");
  });
  $("#listitemb2").swiperight(function() {
//alert("worked");
$.mobile.changePage("#page1");
});
  $("#listitem3").swipeleft(function() {
    $.mobile.changePage("#page4");
  });
  $("#listitemb3").swiperight(function() {
    $.mobile.changePage("#page2");
  });
  $("#listitem4").swipeleft(function() {
    $.mobile.changePage("#page5");
  });
  $("#listitemb4").swiperight(function() {
    $.mobile.changePage("#page3");
  });
$("#listitem5").swipeleft(function() {
    $.mobile.changePage("#page6");
  });
  $("#listitemb5").swiperight(function() {
    $.mobile.changePage("#page4");
  });
  $("#listitem6").swipeleft(function() {
    $.mobile.changePage("#page7");
  });
  $("#listitemb6").swiperight(function() {
    $.mobile.changePage("#page5");
  });
  $("#listitem7").swipeleft(function() {
    $.mobile.changePage("#page8");
  });
  $("#listitemb7").swiperight(function() {
    $.mobile.changePage("#page6");
  });
    $("#listitem8").swipeleft(function() {
    $.mobile.changePage("#page9");
  });
  $("#listitemb8").swiperight(function() {
    $.mobile.changePage("#page7");
  });
      $("#listitem9").swipeleft(function() {
    $.mobile.changePage("#page10");
  });
  $("#listitemb9").swiperight(function() {
    $.mobile.changePage("#page8");
  });
        $("#listitem10").swipeleft(function() {
    $.mobile.changePage("#page11");
  });
  $("#listitemb10").swiperight(function() {
    $.mobile.changePage("#page9");
  });
  $("#listitem11").swipeleft(function() {
    $.mobile.changePage("#page12");
  });
  $("#listitemb11").swiperight(function() {
    $.mobile.changePage("#page10");
  });
    $("#listitem12").swipeleft(function() {
    $.mobile.changePage("#page13");
  });
    $("#listitemb12").swiperight(function() {
    $.mobile.changePage("#page11");
  });
$("#listitemb13").swiperight(function() {
    $.mobile.changePage("#page12");
  });

/*====== platform theme changer ========== */

 $('input[name=theme-options]').change(function() {
          var currentTheme = $('#platform').attr('data-theme');
          var selectedTheme = $(this).val();

//alert('CT '+currentTheme+' ST '+selectedTheme);

$('.ui-body-' + currentTheme).each(function(){
  $(this).removeClass('ui-body-' + currentTheme).addClass('ui-body-' + selectedTheme);    
});

$('.ui-btn-up-' + currentTheme).each(function(){
  $(this).removeClass('ui-btn-up-' + currentTheme).addClass('ui-btn-up-' + selectedTheme);    
});

$('.ui-btn-down-' + currentTheme).each(function(){
  $(this).removeClass('ui-btn-down-' + currentTheme).addClass('ui-btn-down-' + selectedTheme);    
});

$('#platform').find('*[data-theme]').each(function(index){
  $(this).attr('data-theme',selectedTheme);
});

$('#platform').attr('data-theme', selectedTheme).removeClass('ui-body-' + currentTheme).addClass('ui-body-' + selectedTheme).trigger('create');
});

 /*======= Dragging idgets functionality ========*/


    $(function() {
      $( ".column" ).sortable({
        connectWith: ".column",
        handle: ".portlet-header",
        cancel: ".portlet-toggle",
        placeholder: "portlet-placeholder ui-corner-all"
      });

      $( ".portlet" )
      .addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
      .find( ".portlet-header" )
      .addClass( "ui-widget-header ui-corner-all" )
      /* .prepend( "<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>"); */
      .prepend( "<span class='ui-icon ui-icon-arrows portlet-toggle'></span>");


      $( ".portlet-toggle" ).click(function() {
        var icon = $( this );
        icon.toggleClass( "ui-icon-minusthick ui-icon-plusthick" );
        icon.closest( ".portlet" ).find( ".portlet-content" ).toggle();
      });
    });


  function hideResponse(){
    setTimeout(function(){
      $("#response").slideUp("slow", function(){
      });

    }, 2000);}
    $("#response").hide();
    $(function() {

      $(".column").sortable({opacity:0.5, cursor: 'move',update:function(){
        var order = $(this).sortable("serialize")+ '&update=order';
        $.post("<?php echo site_url('dragPlatfom4/dragSave'); ?>", order, function(response){
          $("#response").html(response);
          $("#response").slideDown('slow');
          hideResponse();
        });
      }
    });
    });



  