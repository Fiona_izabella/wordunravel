<?php
/*function to help with adding a button icon*/
function btn_edit ($uri)
{
    return anchor($uri, '<i class="icon-edit"></i>');
}
/*function to help with adding a button icon. Adds javascript to aid in usability*/
function btn_delete ($uri)
{
    return anchor($uri, '<i class="icon-remove"></i>', array(
        'onclick' => "return confirm('You are about to delete a record. This cannot be undone. Are you sure?');"
        //this is javascript to add message about deleteing
    ));
}

?>