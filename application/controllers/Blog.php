<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {




  public function __construct ()
  {
    parent::__construct();
    //need to load the session to use the flashdata and set encryption key in config
    $this->load->library('session');
    $this->data['meta_title'] = config_item('site_name');


  }

  public function index()
  {

// Load the view   
    $this->data['subview'] = 'blog';
    $this->load->view('main_layout', $this->data);
  }

  public function addPost(){


    try
    {
      //if $_POST is not set then make value equal null
      $postTitle = (isset($_POST['postTitle']) ? $_POST['postTitle'] : null);
      $postContent= (isset($_POST['postContent']) ? $_POST['postContent'] : null);
      //escapes special characters in a string for use in an SQL statement
      $postTitle = mysql_real_escape_string($postTitle);
      $postContent = mysql_real_escape_string($postContent);

      mysql_query("INSERT INTO posts (post_title, post_content) VALUES ('$postTitle', '$postContent')");
      echo "SUCCESS";
    }
    catch(Exception $e)
    {
      //log the error
      echo $e->getMessage();
    
    }

  }


}//end of class

