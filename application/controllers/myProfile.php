<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MyProfile extends CI_Controller {


	public function __construct ()
	{
		parent::__construct();
		$this->load->library('session');
		$this->data['meta_title'] = config_item('site_name');
		$this->load->helper('form');
		$this->load->model('profile_m');


	}

	public function index()
	{

        // Load the view
        //get users details from database and pass to view theough $profileDetails object
        $usersIdnumber = $this->session->userdata('id');
		$this->data['profileDetails'] = $this->profile_m->getProfileDetails($usersIdnumber);
		$this->data['subview'] = 'myProfile';
		$this->load->view('main_layout', $this->data);


	}
}

