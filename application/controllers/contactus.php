<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contactus extends CI_Controller {

  public $name;
  public $email;


  public function __construct ()
  {
    parent::__construct();
//need to load the session to use the flashdata and set encryption key in config
    $this->load->library('session');
    $this->data['meta_title'] = config_item('site_name');

  }

  public function index()
  {

// Load the view
    $this->data['name'] = $this->name;
    $this->data['email'] = $this->email;
    $this->data['subview'] = 'contactus';
    $this->load->view('main_layout', $this->data);
  }

  public function sendcontactMail(){

    $this->load->library('form_validation');
//now we can access it as a class and use its methods

//Note: set validation rules with field name, error message, validation rule
    $this->form_validation->set_rules('name', 'Name','trim|required');  
    $this->form_validation->set_rules('email', 'Email','trim|required|valid_email');
    $this->form_validation->set_rules('message', 'Message','trim|required');
    $this->form_validation->set_rules('subject', 'Subject','required');

//we need to run the validation
    if($this->form_validation->run() == FALSE){

//set erors in flashdata to work with a redirect
      $this->session->set_flashdata('errors', validation_errors());


//load the rediect helper
      $this->load->helper('redirect_helper');
      redirect_form_validation(validation_errors(), $this->input->post(), 'contactus');


    } else{
//validation has passed and send email
$name = $this->input->post('name');//same as $_POST['name'];
$email = $this->input->post('email');
$contactsubject = $this->input->post('subject');
$contactmessage = $this->input->post('message');

//configure message string
$message = "This was sent from $name <br/>";
$message .= "There email of $email <br/>";
$message .= "The message is $contactsubject!<br/>";
$message .= "The message is $contactmessage!<br/>";




//set the configuration for sending email with smtp
$config = Array(
//'protocol' => 'smtp',
  /*changed to simple smtp and removed ssl*/
//'smtp_host' => 'smtp.googlemail.com',
  /*'smtp_port' => 465,or try 587*/   
// 'smtp_port' => 25,
// 'smtp_user'=> 'fionka78@googlemail.com',
// 'smtp_pass'=> '***********',
  'mailtype' => 'html'
  );

$this->load->library('email', $config);
$this->email->set_newline("\r\n");
$this->email->from($email, $name);
$this->email->subject('unravel form');
$this->email->to('fionka78@gmail.com');
$this->email->message($message);



if($this->email->send()){

// redirect to current page with flashdata message, the secod param is stored in the 'message';
  $this->session->set_flashdata('message', "Thankyou $name, your email has been sent");
  redirect('contactus');

} else{

  show_error($this->email->print_debugger());
  echo "your email didnt send";
}


}


}



}//end of class
