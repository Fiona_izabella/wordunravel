<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class EditProfile extends CI_Controller {


  public function __construct ()
  {
    parent::__construct();

    parent::__construct();
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->load->library('session');
    //load the model
    $this->load->model('profile_m');
    $this->data['errors'] = array();

  }

  public function index ($id = NULL)
  {

  }

  public function edit ($id = NULL)
  {

    $this->load->library('form_validation');
      // Fetch the user
    if ($id) {
      $this->data['profile'] = $this->profile_m->get($id);
      //if no user can be found with that $id, add error message to the $error array
      count($this->data['profile']) || $this->data['errors'][] = 'profile could not be found';
    }
    else {
    //create a new user object
      $this->data['profile'] = $this->profile_m->get_new();
    }

    $this->form_validation->set_rules('username','Username','required');  
    $this->form_validation->set_rules('email', 'Email','trim|required|valid_email'); 
    $this->form_validation->set_rules('Bdate', 'Birthday','required');  


    //run validation
    if ($this->form_validation->run() == FALSE)
    {

      $this->data['subview'] = 'editProfile';
      $this->load->view('main_layout_admin', $this->data);

    }
    else {

      // Process the form
      //username, password, email, Bdate, sign_up_date
      $data = $this->profile_m->array_from_post(array('username', 'email', 'Bdate'));
      $this->profile_m->save($data, $id);

      //update session values
      $username = $this->input->post('username');
      $this->session->set_userdata('username', $username);

      redirect('myProfile');
    }  


}//end of edit method


}//end of class

