<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SignUpSuccess extends CI_Controller {


		public function __construct ()
	{
		parent::__construct();
		//need to load the session to use the flashdata and set encryption key in config
		 $this->load->library('session');
		 $this->data['meta_title'] = config_item('site_name');
		
		
	}

	public function index()
	{
	
      // Load the view
      $this->data['subview'] = 'signUpSuccess';
      $this->load->view('main_layout', $this->data);
	}
}

