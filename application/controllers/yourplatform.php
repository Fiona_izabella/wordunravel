<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); 

class YourPlatform extends CI_Controller {


		public function __construct ()
	{
		parent::__construct();
		//need to load the session to use the flashdata and set encryption key in config
		 $this->load->library('session');
		 $this->data['meta_title'] = config_item('site_name');
		
		
	}

	public function index(){
	
     }


	function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('home', 'refresh');
 }


}//end of class
