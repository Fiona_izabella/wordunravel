<?php
class Publicfolders extends CI_Controller
{

    public function __construct ()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));

    }

    public function index ()
    {

        
        if($this->session->userdata('logged_in'))
        {
         

         /*
         If user logged in, display the all the public folders in order they were created
         Get all the details of the folders to be displayed.
          */
                $sessionUser =  $this->session->userdata('username');

                $this->load->model('folder_m');
                $this->db->order_by('created', 'desc');
                $this->db->where('folderRole', 'public');
                $this->data['folders'] = $this->folder_m->get();

               //Only allow a user to edit their a folder if they own the folder
                $this->data['allowEdit'] =  $this->folder_m->permitEdit($sessionUser);
                              
                // Load view
                $this->data['subview'] = 'public_folders';
                $this->load->view('main_layout_admin', $this->data);

        } else {

        //If no session, redirect to login page
     redirect('login', 'refresh');
       }

    }



    public function delete ($id)
    {
        $this->load->model('folder_m');
        $this->folder_m->delete($id);
        redirect('wordFolder');
    }



}//end of class