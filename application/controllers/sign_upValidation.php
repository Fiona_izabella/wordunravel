<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sign_upValidation extends CI_Controller {


  public function __construct () {
    parent::__construct();
//need to load the session to use the flashdata and set encryption key in config
    $this->load->library('session');
    $this->data['meta_title'] = config_item('site_name');
    $this->load->model('sign_upValidation_m');

  }

  public function index(){

    $this->load->library('form_validation');
    //now we can access it as a class and use its methods

    $this->form_validation->set_rules('username', 'Username','required|is_unique[users.username]');
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required');
    $this->form_validation->set_rules('date', 'Date', 'required');
    //we need to run the validation
    if($this->form_validation->run() == FALSE){


      $this->session->set_flashdata('errors', validation_errors());
      redirect('sign_up');


    } else{

    //validation has passed. Insert data into database
    $username = $this->input->post('username');
     $password = $this->input->post('password');
    $email = $this->input->post('email');
    $date = $this->input->post('date');
    $signup_date ="";
    


      $this->sign_upValidation_m->insertData($username, $password, $email, $date, $signup_date);
      $this->data['subview'] = 'signUpSuccess';
      $this->load->view('main_layout', $this->data);

}


}



}//end of class
