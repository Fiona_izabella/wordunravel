<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class EditPassword extends CI_Controller {


  public function __construct ()
  {
    parent::__construct();
//need to load the session to use the flashdata and set encryption key in config
    $this->load->library('session');
    $this->data['meta_title'] = config_item('site_name');
    $this->load->model('profile_m');
    $this->load->model('user_m');


  }

  public function index()
  {
    $this->load->library('session');
// Load the view
// get data from session
    $usersIdnumber = $this->session->userdata('id');
    $currentPassword = $this->session->userdata('password');
//get user details with a specific $id
    $this->data['profileDetails'] = $this->profile_m->getProfileDetails($usersIdnumber);


    $this->data['notification'] = "test";
    $this->data['subview'] = 'editPassword';
    $this->load->view('main_layout_admin', $this->data);
  }

/** 
method to edit password. First Need to check thh users original password matches their passeword stored
in the database. Need to pass in an $id parameter. Validation rules use a callback function "check_Database();"
*/

public function editPass($id = NULL){
  $this->load->library('session');
  $usersIdnumber = $this->session->userdata('id');


  $this->data['password'] = $this->profile_m->getProfilePassword($usersIdnumber);
  $passwordForCheck = $this->profile_m->getProfilePassword($usersIdnumber);
  $currentPassword = $this->profile_m->getProfilePassword($usersIdnumber);
  $this->data['check'] = $this->profile_m->getProfilePassword($usersIdnumber);


  $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
  $this->form_validation->set_rules('new_password', 'new password', 'trim|required|xss_clean');

  if($this->form_validation->run() == FALSE)
  {
//Field validation failed.  User redirected to login page
    $this->data['notification'] = "failed";
    $this->data['profileDetails'] = $this->profile_m->getProfileDetails($usersIdnumber);
    $this->data['subview'] = 'editPassword';
    $this->load->view('main_layout_admin', $this->data);
  }
  else
  {

    $new_password = $this->input->post('new_password');
    $password_reset = md5($new_password);
    $this->user_m->resetPassword($password_reset, $usersIdnumber);
    redirect('myProfile');

  }




}//end of

function check_password($username)
{
  //Field validation succeeded.  Now must Validate against database
  $username= $this->session->userdata('username');
  $password = $this->input->post('password');

   //query the database
  $result = $this->user_m->loginTest($username, $password);

  if($result)
  {

    return TRUE;
  }
  else
  {
    $this->form_validation->set_message('check_database', 'Invalid password');
    return false;
  }
}

//using this one
function check_database(){
//Note: need to get the password out of the dtabase and not the sesson!

  $usersIdnumber = $this->session->userdata('id');
  $currentPassword = $this->profile_m->getProfilePassword($usersIdnumber);
  $check = $currentPassword->password;


  $passwordCheck = $this->input->post('password');
  $passwordCheckHashed = md5($passwordCheck);

//if passwords match return true and pass to validation rules in editPass method()
  if ($passwordCheckHashed ==  $check){

    return TRUE;

  } else {
    $this->form_validation->set_message('check_database', 'Your password is Not correct please try again');
    return false;
  }


}



}//end of clas



