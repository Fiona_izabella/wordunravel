<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class WordFolderEdit extends CI_Controller {



		 public function __construct ()
    {
        parent::__construct();
        //load the models and helpers
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('word_m');
        $this->data['errors'] = array();
    }

    public function index ($id = NULL)
    {

        $this->data['folders'] = $this->word_m->get();
        
            $this->load->library('form_validation');
        // Fetch a folder or set a new one
        if ($id) {
            $this->data['folder'] = $this->word_m->get($id);
            //if no folder can be found with that $id, add error message to the $error array
            count($this->data['folder']) || $this->data['errors'][] = 'Folder could not be found';
        }
        else {
            //create a new user object
            $this->data['folder'] = $this->word_m->get_new();
        }
        //set rules
        $this->form_validation->set_rules('name', 'Name','trim|required');  
        $this->form_validation->set_rules('wordOne', 'wordOne','trim|required');  
        

        
        // Process the form
        if ($this->form_validation->run() == TRUE) {
            //if passes validation get the values, save to database.
            $data = $this->word_m->array_from_post(array('name', 'wordOne', 'wordTwo', 'wordThree','wordFour', 'wordFive','wordSix'));
            $this->word_m->save($data, $id);
            redirect('wordFolder');
        }  else{

            $this->data['subview'] = 'wordFolder_edit';
            $this->load->view('main_layout', $this->data);
 
       }
        
        
    }

    public function delete ($id)
    {
        $this->word_m->delete($id);
        redirect('wordFolder');
    }   


}//end of class
