<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TestResults extends CI_Controller {


    public function __construct ()
    {
        parent::__construct();
        $this->load->library('session');
        $this->data['meta_title'] = config_item('site_name');
        $this->load->model('profile_m');


    }

    public function index()
    {   

      //user can only access page if logged in
      if($this->session->userdata('logged_in'))
        { 

        //get user details from session and pass to view
        $usersId = $this->session->userdata('id');
        $this->data['usersName'] = $this->session->userdata('username');
        $this->data['usersId'] = $usersId;

        //get form results, work out the results, and pass to view
        
        $countvisualResults =  $_POST['visual'];
        $a = array_sum($countvisualResults);
        $this->data['totalCountOfVisual']= array_sum($countvisualResults);

        $countaudioResults =  $_POST['auditive'];
        $b = array_sum($countaudioResults);
        $this->data['totalCountOfAudio']= array_sum($countaudioResults);

        $countKineticResults =  $_POST['kinesthetic'];
        $c = array_sum($countKineticResults);
        $this->data['totalCountOfKinetic']= array_sum($countKineticResults);

        $resultsArray = array($a, $b, $c);
        $this->data['combinedArray']= $resultsArray;

        $topScore = max($a, $b, $c);
        $this->data['topScore']= $topScore;

        $test= array("visual"=>$a,"audio"=> $b,"kinetic"=>"$c");
        $this->data['test']= $test ;

        $this->data['topScorePrint']= $topScorePrint ;

        $bestScore = max($a, $b, $c);
        $this->data['bestScore']= $bestScore ;

        //switch statement print the users learning style to page view
        switch ($topScore) {
            case $a:
            $FunctionResultTest = "visual";
            break;

            case $b:
            $FunctionResultTest = "auditory";
            break;

            case $c:
            $FunctionResultTest = "kinesthetic";
            break;

            default:
            $FunctionResultTest = "didnt work";
            break;
        }

        $this->data['FunctionResultTest']= $FunctionResultTest ;
        $this->profile_m->update_vakResults($FunctionResultTest, $usersId);

        //load view and pass all above information through $data variable
        $this->data['subview'] = 'testResults';
        $this->load->view('main_layout', $this->data);

        } else {

             //If user is not logged in, redirect to login page
            redirect('loginTest', 'refresh');
        }

    }//end of index method



}//end of class

