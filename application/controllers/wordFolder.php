<?php
class WordFolder extends CI_Controller
{

    public function __construct ()
    {
        parent::__construct();
        //load the helpers
        $this->load->helper('form');
         $this->load->helper(array('form', 'url'));

    }

    public function index ()
    {
      
        //if user logged in
        if($this->session->userdata('logged_in'))
        {
        
            $this->load->model('folder_m');
            $this->db->order_by('created', 'desc');
           
            $sessionUser =  $this->session->userdata('username');
            $this->db->where(array('folderOwner'=> $sessionUser));

            $this->data['folders'] = $this->folder_m->get();

            // Load view
            $this->data['subview'] = 'word_index';
            $this->load->view('main_layout_admin', $this->data);

        } else {

        //If no session, redirect to login page
     redirect('login', 'refresh');
       }

    }

    public function edit ($id = NULL)
    {
          
         $this->load->model('folder_m');
           
        // Fetch a folder or set a new one
        if ($id) {
            $this->data['folder'] = $this->folder_m->get($id);
          
     
        }
        else {
            //create a new folder object
            $this->data['folder'] = $this->folder_m->get_new();
        }

        $rules = $this->folder_m->rules;
        $this->form_validation->set_rules($rules);
      
    
        
        if ($this->form_validation->run() == FALSE)
        {

      
             $this->data['subview'] = 'wordFolder_edit';
             $this->load->view('main_layout_admin', $this->data);
           
        }
        
        // Process the form

        else {
            //if passes validation get the values, save to database.
            $data = $this->folder_m->array_from_post(array('name', 'folderOwner', 'folderRole','wordOne', 'wordTwo', 'wordThree','wordFour', 'wordFive','wordSix','difficulty'));
            $this->folder_m->save($data, $id);
            redirect('myFolders');
        } 
            

        
    }

    public function delete ($id)
    {
        $this->load->model('folder_m');
        $this->folder_m->delete($id);
        redirect('myFolders');
    }





    
}