<?php
class User_M extends CI_Model
{
	/** @var string [create the table name called users] */
	protected $_table_name = 'users';
	/** @var string [variable which decides how to order the data] */
	protected $_order_by = 'name';
	/** @var array [create the rules and save in array] */
	public $rules = array(
		'email' => array(
			'field' => 'email', 
			'label' => 'Email', 
			'rules' => 'trim|required|valid_email|xss_clean'
			), 
		'password' => array(
			'field' => 'password', 
			'label' => 'Password', 
			'rules' => 'trim|required'
			)
		);

	function __construct ()
	{
		parent::__construct();
	}

	function loginTest($username, $password)
	{

//$password = md5($password);
		$this->db->select('id, username, password');
		$this->db->from('users');
		$this->db->where('username', $username); 
		$this->db->where('password', $password);
		$this->db->limit(1);

		$query =$this->db-> get();

		if($query -> num_rows() == 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}

	}

//reset password
	function resetPassword($new_password, $id){
		$data = array('password' => $new_password);
		$this->db->where('id', $id);
		$this->db->update('users', $data); 
	}

//get user password
	function getUserPassword($id){
		$this->db->select('password');
		$this->db->where('id', $id);
		$query = $this->db->get('users');
	}


/** [method to get data from database using the 'where' query. 
$where could be an array. if you only want one object returned then you have TRUE as your second parameter] */
public function get_by($where, $single = FALSE){
	$this->db->where($where);
//we need to pass in the where statement
	return $this->get(NULL, $single);
}

/**
* [method for logging in.
*get email, role and password from database.
*if user with above credientials exists, set the session with the $data array]
* @return [set the session and return a boolean] [description]
*/
public function login ()
{
	$user = $this->get_by(array(
		'username' => $this->input->post('username'),
		'password' => $this->input->post('password')
		), TRUE);

	if (count($user)) {
//if a user with the above credientials is found.
// Log in user
		$data = array(
			'name' => $user->name,
			'email' => $user->email,
			'id' => $user->id,
			'role' => $user->role,
			'loggedin' => TRUE,
			);
//add the data to the session array
//return true if it works otherwise return false
		$this->session->set_userdata($data);
		return true;

	} else {

		return false;
	}
}

/** [log out user by destroying the session] */
public function logout ()
{
	$this->session->sess_destroy();
}

/*method to determine whether user is logged in or not*/
/** [returns boolean value] */
public function loggedin ()
{
	return (bool) $this->session->userdata('loggedin');
}

// Check if username exists
// this function works with ajax call
public function username_exists($name)
{
	$this->db->where('name', $name);
	$this->db->from('users');
	$query = $this->db->get();

	if ($query->num_rows() > 0) {
		return TRUE;
		/*print_r($query);*/
	} else {
		return FALSE;
	}
}


// Check if username matches the username of a certain id
// // this function works with ajax call
public function username_existsWithId($name, $id)
{
	$array = array('name' => $name, 'id' => $id);
	$this->db->where($array); 
	$this->db->from('users');
	$query = $this->db->get();

	if ($query->num_rows() > 0) {
return FALSE;//return false if the user with the id HAS that name
} else {
return TRUE;//return true if user with the id does NOT have that name
}
}


// Check if email exists
// this function works with ajax call
//TODO: need to incorporate this method
public function email_exists($email)
{
	$this->db->where('email', $email);
	$this->db->from('users');
	$query = $this->db->get();

	if ($query->num_rows() > 0) {
		return TRUE;
	} else {
		return FALSE;
	}
}


//Use this so that we always have a valid user object and dont get errors if there no users
public function get_new(){
	$user = new stdClass();
	$user->name = '';
	$user->email = '';
	$user->role = '';
	$user->password = '';
	return $user;
}

/*method used with function login() above to hash password for security*/
public function hash ($string)
{
	return hash('sha512', $string . config_item('encryption_key'));
}
}