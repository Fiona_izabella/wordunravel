<?php
class folder_m extends CI_Model
{

//set up the basic variables with default values
	
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';//intval casts any input as an integar
	protected $_timestamps = TRUE;
	protected $_table_name = 'wordFolders';
	/** @var string [variable which decides how to order the data] */
	protected $_order_by = 'id';
	/** @var array [create the rules and save in array] */
	public $rules = array(
		'name' => array(
			'field' => 'name', 
			'label' => 'Name', 
			'rules' => 'trim|required|xss_clean'
		),
		'wordOne' => array(
			'field' => 'wordOne', 
			'label' => 'WordOne', 
			'rules' => 'trim|required|alpha|xss_clean'
		),
		'wordTwo' => array(
			'field' => 'wordTwo', 
			'label' => 'WordTwo', 
			'rules' => 'trim|required|xss_clean'
		),
		'wordThree' => array(
			'field' => 'wordThree', 
			'label' => 'WordThree', 
			'rules' => 'trim|required|xss_clean'
		),
		'wordFour' => array(
			'field' => 'wordFour', 
			'label' => 'WordFour', 
			'rules' => 'trim|required|xss_clean'
		),
		'wordFive' => array(
			'field' => 'wordFive', 
			'label' => 'WordFive', 
			'rules' => 'trim|required|xss_clean'
		),
		'wordSix' => array(
			'field' => 'wordSix', 
			'label' => 'WordSix', 
			'rules' => 'trim|required|xss_clean'
		),
		
	);


	function __construct ()
	{
		parent::__construct();
	}


	/** [get value from $fields array using $_POST and store in $data array] */
	public function array_from_post($fields){
		$data = array();
		foreach ($fields as $field) {
			$data[$field] = $this->input->post($field);
		}
		return $data;
	}


	public function get($id = NULL, $single = FALSE){
		
		if ($id != NULL) {
			//if we have an id then we need a single record (row)
			$filter = $this->_primary_filter;
			$id = $filter($id);//added security by filering $id
			$this->db->where($this->_primary_key, $id);
			$method = 'row';
		}

		elseif($single == TRUE) {
			$method = 'row';
			
		}

		else {
			$method = 'result';
			//else we need all folder records
		}
		
		if (!count($this->db->ar_orderby)) {
			$this->db->order_by($this->_order_by);
		}
		return $this->db->get($this->_table_name)->$method();
		//returns result for (all), or a row for a single value
	}


	/** [method to save data into database. need id to be set to NULL as default] */
	public function save($data, $id = NULL){
		/** 
		 * if you pass an $id it will be an update, otherwise it will be an insert.
		 * So if you want to update an element with an id of three you do the following...
		 * $this->model->save($data, 3); if the second parameter isnt there it will insert all $data
		 */
		
		// Set timestamp with 'created' and 'modified' fields being set to timestamp value
		if ($this->_timestamps == TRUE) {
			$now = date('Y-m-d H:i:s');
			$id || $data['created'] = $now;
			$data['modified'] = $now;
		}
		
		// Insert
		if ($id === NULL) {
			!isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
			$this->db->set($data);
			$this->db->insert($this->_table_name);
			$id = $this->db->insert_id();//set the $id value using last insert id php function
		}
		// Update
		else {
			$filter = $this->_primary_filter;//filter the primary key
			$id = $filter($id);
			$this->db->set($data);
			$this->db->where($this->_primary_key, $id);
			$this->db->update($this->_table_name);
		}
		
		return $id;
	}


	/** [function to delete item from database by its $id. add limit to only delete one item at a time] */
	public function delete($id){
		$filter = $this->_primary_filter;
		$id = $filter($id);
		
		if (!$id) {
			return FALSE;
		}
		$this->db->where($this->_primary_key, $id);
		$this->db->limit(1);
		$this->db->delete($this->_table_name);
	}



	//so that we always have a valid folder object and dont get errors if there no users
	public function get_new(){
		$folder = new stdClass();
		$folder->name = '';
		$folder->wordOne = '';
		$folder->wordTwo = '';
		$folder->wordThree = '';
		$folder->wordFour = '';
	    $folder->wordFour = '';
	     $folder->wordFive = '';
	      $folder->wordSix = '';
	      $folder->folderOwner = '';
	      $folder->folderRole = '';
	      $folder->difficulty ='';
		return $folder;
	}


  /*get username from folderOwnerId*/
  function getFolderOwnerId($id){
    $this->db->select('folderOwner');
    $this->db->where('id', $id);
    $query = $this->db->get('wordFolders');
    //this returns an object which i can then call with echo $folder->username;
     return $query->row();//return as an object array
     

  }

	public function permitEdit($sessionUser = NULL){
		
		if ($sessionUser != NULL) {
		
			$this->db->where(array('folderOwner'=> $sessionUser, 'folderRole'=>'public'));
			$this->db->order_by('created', 'desc');
			
		
		}
		return $this->db->get($this->_table_name)->result();
		//returns result for (all), or a row for a single value
	}

    

}