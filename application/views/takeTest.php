<div data-role="page" id="home" date-theme="a">

	<div data-role="header">
		<h1 class="maintitle">Vak test for <?php echo $usersName ; ?></h1>
	</div><!-- /header -->

	<div data-role="content">	
  <div role="main" class="ui-body ui-body-a ui-corner-all" >

  <!--debugging code-->
<?php //print_r($OverallResult); ?>
<?php //print_r($combinedArray); ?>
<?php //echo "user id $usersId "; ?>
<?php //echo "visual results: $totalCountOfVisual"  ;?>
<?php //echo "audio results: $totalCountOfAudio"  ;?>
<?php //echo "Kinesthetic results: $totalCountOfKinetic"  ;?>
<?php //echo "top score: $topScore"  ;?>
<?php //print_r($test); ?>
<?php //print_r($kinestheticResults); ?>
<?php //echo "this works?: $FunctionResultTest"  ;?>
 <!--end of debugging code-->
 
<h1>Take the Vak test</h1>
<div>		
  <p>
    Each statement has three options.
    Prioritise each option as follows:
  </p>
  <ul>
    <li>3 = is the most accurate description</li>
    <li>2 = characterises me to some extent</li>
    <li>1 = is the description that does not characterise me very well</li>
  </ul>
</div>

<form id="vak" name="vak" method="POST" action="takeTest/getResults">

<p class="vakQuestion">1. THE EASIEST WAY TO LEARN IS?:</p>

    <fieldset data-role="controlgroup" data-type="horizontal">
    <label for="Select 1A">Select 1A</label>
    <select name="visual[]" id="visual[]">
       <option value="1">A 1</option>
        <option value="2">A 2</option>
        <option value="3">A 3</option>
    </select>


    <label for="Select B">Select 1B</label>
    <select name="auditive[]" id="auditive[]">
       <option value="1">B 1</option>
        <option value="2">B 2</option>
        <option value="3">B 3</option>
    </select>




    <label for="Select 1C">Select 1C</label>
    <select name="kinesthetic[]" id="kinesthetic[]">
          <option value="1">C 1</option>
        <option value="2">C 2</option>
        <option value="3">C 3</option>
    </select>
</fieldset>
<p class="emphasize">(A)  by viewing. reading, and observing how the others carry out certain tasks </p>
<p class="emphasize">(B)  by listening, discussing and doing according to verbal instructions </p>
<p class="emphasize">(C)  by doing and experimenting by myself </p>

<!--question 2-->

<p class="vakQuestion">2. WHEN BUYING CLOTHES I USUALLY MAKE THE DECISION ON THE BASIS OF:</p>


    <fieldset data-role="controlgroup" data-type="horizontal">
<label for="Select 2A">Select 2A</label>
     <select name="visual[]" id="visual[]">
      <option value="1">A 1</option>
        <option value="2">A 2</option>
        <option value="3">A 3</option>
    </select>

   <label for="Select 2B">Select 2B</label>
     <select name="auditive[]" id="auditive[]">
      <option value="1">B 1</option>
        <option value="2">B 2</option>
        <option value="3">B 3</option>
    </select>

      <label for="Select 2C">Select 2C</label>
   <select name="kinesthetic[]" id="kinesthetic[]">
    <option value="1">C 1</option>
        <option value="2">C 2</option>
        <option value="3">C 3</option>
    </select>
</fieldset>
<p class="emphasize">(A)  by their appearance and looks </p>
<p class="emphasize">(B)  their practicality and use  </p>
<p class="emphasize">(C)  how comfortable they are or how they feel </p>
  

<!--question 3-->

<p class="vakQuestion">3. I SPEAK:</p>


    <fieldset data-role="controlgroup" data-type="horizontal">
  <label for="Select 3A">Select 3A</label>
     <select name="visual[]" id="visual[]">
       <option value="1">A 1</option>
        <option value="2">A 2</option>
        <option value="3">A 3</option>
    </select>


     <label for="Select 3B">Select 3B</label>
    <select name="auditive[]" id="auditive[]">
      <option value="1">B 1</option>
        <option value="2">B 2</option>
        <option value="3">B 3</option>
    </select>


   <label for="Select 3C">Select 3C</label>
     <select name="kinesthetic[]" id="kinesthetic[]">
       <option value="1">C 1</option>
        <option value="2">C 2</option>
        <option value="3">C 3</option>
    </select>
</fieldset>
<p class="emphasize">(A)  fast</p>
<p class="emphasize">(B)  at the medium rate </p>
<p class="emphasize">(C)  slowly </p>
<!--question 4-->

<p class="vakQuestion">4. IN NEW SURROUNDINGS AND ENVIROMENT I USUALLY FIND THE PLACES :</p>


    <fieldset data-role="controlgroup" data-type="horizontal">
   <label for="Select 4A">Select 4A</label>
     <select name="visual[]" id="visual[]">
         <option value="1">A 1</option>
        <option value="2">A 2</option>
        <option value="3">A 3</option>
    </select>



 
   <label for="Select 4B">Select 4B</label>
   <select name="auditive[]" id="auditive[]">
    <option value="1">B 1</option>
        <option value="2">B 2</option>
        <option value="3">B 3</option>
    </select>



 
    <label for="Select 4C">Select 4C</label>
  <select name="kinesthetic[]" id="kinesthetic[]">
      <option value="1">C 1</option>
        <option value="2">C 2</option>
        <option value="3">C 3</option>
    </select>
</fieldset>
<p class="emphasize">(A)  with the help of a map </p>
<p class="emphasize">(B)  by asking for directions </p>
<p class="emphasize">(C)  by figuring out the places and directions on my own </p>

<!--question 5-->

<p class="vakQuestion">5. WHEN I TRY TO RECALL CERTAIN PERSON I REMEMBER :</p>

    
    <fieldset data-role="controlgroup" data-type="horizontal">
    <label for="Select 5A">Select 5A</label>
    <select name="visual[]" id="visual[]">
       <option value="1">A 1</option>
        <option value="2">A 2</option>
        <option value="3">A 3</option>
    </select>


    
 
    <label for="Select 5B">Select 5B</label>
   <select name="auditive[]" id="auditive[]">
        <option value="1">B 1</option>
        <option value="2">B 2</option>
        <option value="3">B 3</option>
    </select>

      <label for="Select 5C">Select 5C</label>
  <select name="kinesthetic[]" id="kinesthetic[]">
       <option value="1">C 1</option>
        <option value="2">C 2</option>
        <option value="3">C 3</option>
    </select>
</fieldset>
<p class="emphasize">(A)  their faces</p>
<p class="emphasize">(B)  their names </p>
<p class="emphasize">(C)  the atmosphere and emotional aspects</p>

<!--question 6-->

<p class="vakQuestion">6. IF I COULD CHOOSE MYSELF AN IDEAL HOME, IT WOULD :</p>

 
    <fieldset data-role="controlgroup" data-type="horizontal">
     <label for="Select 6A">Select 6A</label>
    <select name="visual[]" id="visual[]">
      <option value="1">A 1</option>
        <option value="2">A 2</option>
        <option value="3">A 3</option>
    </select>



   
    <label for="Select 6B">Select 6B</label>
   <select name="auditive[]" id="auditive[]">
    <option value="1">B 1</option>
        <option value="2">B 2</option>
        <option value="3">B 3</option>
    </select>


   
    <label for="Select 6C">Select 6C</label>
   <select name="kinesthetic[]" id="kinesthetic[]">
      <option value="1">C 1</option>
        <option value="2">C 2</option>
        <option value="3">C 3</option>
    </select>
</fieldset>
<p class="emphasize">(A)  please my easthetic taste</p>
<p class="emphasize">(B)  is peaceful, calm and noiseless</p>
<p class="emphasize">(C)  be a place where it is easy to move around </p>

<!--question 7-->

<p class="vakQuestion">7. I IDENTIFY WORDS :</p>

 
    <fieldset data-role="controlgroup" data-type="horizontal">
  <label for="Select 7A">Select 7A</label>
    <select name="visual[]" id="visual[]">
      <option value="1">A 1</option>
        <option value="2">A 2</option>
        <option value="3">A 3</option>
    </select>


  
    
     <label for="Select 7B">Select 7B</label>
   <select name="auditive[]" id="auditive[]">
     <option value="1">B 1</option>
        <option value="2">B 2</option>
        <option value="3">B 3</option>
    </select>


 
    <label for="Select 7C">Select 7C</label>
     <select name="kinesthetic[]" id="kinesthetic[]">
      <option value="1">C 1</option>
        <option value="2">C 2</option>
        <option value="3">C 3</option>
    </select>
</fieldset>
<p class="emphasize">(A)  as visual or mental images</p>
<p class="emphasize">(B)  by their sound </p>
<p class="emphasize">(C)  by their taste/feel when I pronounce them </p>

<!--question 8-->

<p class="vakQuestion">8. DURING LECTURES AND MEETINGS :</p>


    <fieldset data-role="controlgroup" data-type="horizontal">
     <label for="Select 8A">Select 8A</label>
    <select name="visual[]" id="visual[]">
      <option value="1">A 1</option>
        <option value="2">A 2</option>
        <option value="3">A 3</option>
    </select>


   
   
     <label for="Select 8B">Select 8A</label>
  <select name="auditive[]" id="auditive[]">
       <option value="1">B 1</option>
        <option value="2">B 2</option>
        <option value="3">B 3</option>
    </select>


    
 
    <label for="Select 8C">Select 8C</label>
    <select name="kinesthetic[]" id="kinesthetic[]">
        <option value="1">C 1</option>
        <option value="2">C 2</option>
        <option value="3">C 3</option>
    </select>
</fieldset>
<p class="emphasize">(A)  I often write down notes and use mind maps</p>
<p class="emphasize">(B)  I enjoy discussion and arguments </p>
<p class="emphasize">(C)  I look for action, do not like to sit in one place for  </p>

<!--question 9-->

<p class="vakQuestion">9. I OFTEN ASK :</p>

  
    <fieldset data-role="controlgroup" data-type="horizontal">
      <label for="Select 9A">Select 9A</label>
   <select name="visual[]" id="visual[]">
        <option value="1">A 1</option>
        <option value="2">A 2</option>
        <option value="3">A 3</option>
    </select>


  
  <label for="Select 9B">Select 9B</label>
    <select name="auditive[]" id="auditive[]">
      <option value="1">B 1</option>
        <option value="2">B 2</option>
        <option value="3">B 3</option>
    </select>



   <label for="Select 9C">Select 9C</label>
 <select name="kinesthetic[]" id="kinesthetic[]">
        <option value="1">C 1</option>
        <option value="2">C 2</option>
        <option value="3">C 3</option>
    </select>
</fieldset>
<p class="emphasize">(A)  what does it look like?</p>
<p class="emphasize">(B)  what does it sound like?</p>
<p class="emphasize">(C)  what does it feel like? </p>

<!--question 10-->

<p class="vakQuestion">10. I'M INSPIRED AND ENERGISED :</p>

    
    <fieldset data-role="controlgroup" data-type="horizontal">
     <label for="Select 10A">Select 10A</label>
    <select name="visual[]" id="visual[]">
       <option value="1">A 1</option>
        <option value="2">A 2</option>
        <option value="3">A 3</option>
    </select>



    <label for="Select 10B">Select 10B</label>
    <select name="auditive[]" id="auditive[]">
       <option value="1">B 1</option>
        <option value="2">B 2</option>
        <option value="3">B 3</option>
    </select>


 

    <label for="Select 10C">Select 10C</label>
   <select name="kinesthetic[]" id="kinesthetic[]">
     <option value="1">C 1</option>
        <option value="2">C 2</option>
        <option value="3">C 3</option>
    </select>
</fieldset>
<p class="emphasize">(A)  by beatiful sights</p>
<p class="emphasize">(B)  by good music and sounds </p>
<p class="emphasize">(C)  by sports and physical exercise </p>

<!--question 11-->

<p class="vakQuestion">11. WHEN I TELL A STORY :</p>


    <fieldset data-role="controlgroup" data-type="horizontal">

   <label for="Select 11A">Select 11A</label>
    <select name="visual[]" id="visual[]">
       <option value="1">A 1</option>
        <option value="2">A 2</option>
        <option value="3">A 3</option>
    </select>
<!-- </fieldset> -->


   <!--  <fieldset data-role="controlgroup" data-type="horizontal"> -->
     <label for="Select 11B">Select 11B</label>
   <select name="auditive[]" id="auditive[]">
      <option value="1">B 1</option>
        <option value="2">B 2</option>
        <option value="3">B 3</option>
    </select>
<!-- </fieldset> -->

     <label for="Select 11C">Select 11C</label>
   <select name="kinesthetic[]" id="kinesthetic[]">
     <option value="1">C 1</option>
        <option value="2">C 2</option>
        <option value="3">C 3</option>
    </select>
</fieldset>
<p class="emphasize">(A)  I accompany the storytelling with my hands and tell all facts very explicitly </p>
<p class="emphasize">(B)  I proceed very systematically from A to B to C </p> 
<p class="emphasize">(C)  I am rather calm however I would rather show something than speak </p>
<!--question 12-->

<p class="vakQuestion">12. MY POSE IS  :</p>

    <fieldset data-role="controlgroup" data-type="horizontal">
  <label for="Select 12A">Select 12A</label>
    <select name="visual[]" id="visual[]">
      <option value="1">A 1</option>
        <option value="2">A 2</option>
        <option value="3">A 3</option>
    </select>
<!-- </fieldset> -->


    <label for="Select 12B">Select 12B</label>
 <select name="auditive[]" id="auditive[]">
      <option value="1">B 1</option>
        <option value="2">B 2</option>
        <option value="3">B 3</option>
    </select>
<!-- </fieldset> -->


    <label for="Select 12C">Select 12C</label>
    <select name="kinesthetic[]" id="kinesthetic[]">
      <option value="1">C 1</option>
        <option value="2">C 2</option>
        <option value="3">C 3</option>
    </select>
</fieldset>
<p class="emphasize">(A)  Upright, and eye-contact is very important </p>
<p class="emphasize">(B)  rather calm and relaxed, and eye-contact is not very important </p> 
<p class="emphasize">(C)  relaxed and comfortable </p>



<button data-theme="a" id="submit" type="submit">Submit</button>
<a href="#" data-rel="back" class="ui-btn ui-shadow">Cancel</a>

  
	
	<div data-role="footer">
		<h4>Footer content</h4>
	</div><!-- /footer -->
</div>	<!--content-->
</div><!-- /page -->