<div data-role="page" date-theme="a">

	<div data-role="content">	
		<div class="ui-body ui-body-a ui-corner-all">

			<h1 class="maintitle centreText"><?php echo $this->session->userdata('username'); ?>'s Profile</h1>
			<!--degugging code-->
			<!--  <h1 class="maintitle">password: <?php //echo $this->session->userdata('password'); ?></h1> -->
			<?php //print_r($profileDetails); ?>
			<!--end of degugging code-->

			<ul data-role="listview">
				<?php foreach ($profileDetails as $d): ?>
					<li class="ui-icon-star ui-btn-icon-right">Your name: <?php echo $d['username']; ?></li>
					<li class="centreText ui-icon-star ui-btn-icon-right">your password: ******<?php //echo $d['password']; ?></li>
					<li class="centreText ui-icon-star ui-btn-icon-right">Your email: <?php echo $d['email']; ?></li>
					<li class="centreText ui-icon-star ui-btn-icon-right">Your learning style: <?php echo $d['vakTestResults']; ?></li>

					<?php $time = strtotime($d['Bdate']); ?>
					<?php $bday = date('d/m/Y', $time); ?>
					<li class="centreText ui-icon-star ui-btn-icon-right">Your birthday: <?php echo $bday; ?></li>

				</ul>

			<?php endforeach; ?>

		</div><!-- /content -->
		<div class="ui-grid-b">
			<div class="ui-block-a"><a href="platform" data-ajax="false"><button type="button" data-theme="a" data-icon="arrow-l">Platform</button></a></div>
			<div class="ui-block-b"><a href="editProfile/edit/<?php echo $d['id'];?>" data-ajax="false"><button type="button" data-theme="a" data-icon="arrow-l">EditProfile</button></a></div>
			<div class="ui-block-c"><a href="editPassword/editPass/<?php echo $d['id'];?>" data-ajax="false"><button type="button" data-theme="a">Edit password</button></a></div>     
		</div><!-- /grid-b -->


	</div>	
</div><!-- /page -->