<!-- Start of first page "help_menu" -->
<div data-role="page" id="instructions" data-theme="a">

  <div role="main" class="ui-content" >



    <a data-role="button" data-theme="a" href="#about_menu" data-rel="dialog" data-transition="pop" data-icon="arrow-r" data-iconpos="right" class="aboutBtn">
      <div class="ui-grid-b">
        <div class="ui-block-a">
          <img src="assets/images/myIcons/lightbulb-o.png" class="iconleft"/> 
        </div>
        <div class="ui-block-b">
          About
        </div>
        <div class="ui-block-c">
        </div>
      </div>
    </a>


    <a data-role="button" data-theme="a" href="#FandQ" data-rel="dialog" data-transition="pop" data-icon="arrow-r" data-iconpos="right" class="aboutBtn">
      <div class="ui-grid-b">
        <div class="ui-block-a">
          <img src="assets/images/myIcons/info-circle.png" class="iconleft"/> 
        </div>
        <div class="ui-block-b">
          F and Q
        </div>
        <div class="ui-block-c">
        </div>
      </div>
    </a>

    <a data-role="button" data-transition="slide" data-theme="a" href="#objectives_menu" data-icon="arrow-r" data-iconpos="right" class="aboutBtn">
      <div class="ui-grid-b">
        <div class="ui-block-a">
          <img src="assets/images/myIcons/info-circle.png" class="iconleft"/> 
        </div>
        <div class="ui-block-b">
          Objectives
        </div>
        <div class="ui-block-c">
        </div>
      </div>
    </a>

    <a data-role="button" data-transition="slide" data-theme="a" href="contactus" data-ajax="false" rel="external" data-icon="arrow-r" data-iconpos="right">
      <div class="ui-grid-b">
        <div class="ui-block-a">
          <img src="assets/images/myIcons/envelope.png" class="iconleft"/> 
        </div>
        <div class="ui-block-b">
          Contact us
        </div>
        <div class="ui-block-c">
        </div>
      </div>
    </a>
    <p class="aboutBtn"hello</p>
      <script>
        $("#infoPopup" ).bind( "click", function(event) {
          console.log('Button has been clicked');
          alert("fiixed");
        });
      </script>




    </div><!-- /content -->


    <p id="testToSpeech">Select to her about us, fact and questions, learn about our goals and objects or send us an email!</p>
    <!--audio element-->
    <p>
      <input type="image" class="audioBtn" id="verify" data-theme="a" alt="image for audio button"/>
      <div id="audioholder" style="height:0;"></div>
    </p>


    <footer data-role="footer">
      <div data-role="navbar" data-iconpos="left">
        <ul id="footerText">
          <li><a href="#" data-icon="bookmark">Bookmark</a></li>
          <li><a href="#" data-icon="facebook" class="ui-btn-active">Facebook</a></li>
          <li><a href="platform" data-icon="user" data-ajax = "false">Platform</a></li>
          <li><a href="contactus" data-icon="envelope" data-transition="slide">Contact</a></li>

        </ul>

      </div><!-- /navbar -->
    </footer><!-- /footer -->

  </div><!-- end of page help_menu one -->

  <!--****pop up ************************ pop up **********************************************-->

  <div data-role="page" id="infoPopup">

    <!-- <div data-role="popup" id="infoPopup"> -->

    <div role="main" class="ui-content">
      <a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>
      Our Vak test is a set of question which helps determine your learning style.
      Once you know your learning style you can use the knowledge to make decisions which will
      speed up and help your learning and studies!
    </div><!-- /content -->

  </div>


  <!--**** PAGE ************************ about_menu page **********************************************-->


  <!-- Start of #about_menu -->
  <div data-role="page" id="about_menu" data-theme="a">

    <div role="main" class="ui-content" >



      <a data-role="button" data-theme="a" href="#text_Popup" data-rel="dialog" data-transition="pop" data-icon="arrow-r" data-iconpos="right" class="aboutBtn">
        <div class="ui-grid-b">
          <div class="ui-block-a">
            <img src="assets/images/myIcons/pencil.png" class="iconleft"/> 
          </div>
          <div class="ui-block-b">
            About us <em> ( Text ) </em>
          </div>
          <div class="ui-block-c">
          </div>
        </div>
      </a>


      <a data-role="button" data-theme="a" href="aboutSlides" rel="external" data-transition="slide" data-icon="arrow-r" data-iconpos="right" class="aboutBtn">
        <div class="ui-grid-b">
          <div class="ui-block-a">
            <img src="assets/images/myIcons/picture-o.png" class="iconleft"/> 
          </div>
          <div class="ui-block-b">
            About us Slides <em> ( Slides ) </em>
          </div>
          <div class="ui-block-c">
          </div>
        </div>
      </a>

      <a data-role="button" data-transition="pop" a href="#text_Video" data-rel="dialog"  data-icon="arrow-r" data-iconpos="right" class="aboutBtn">
        <div class="ui-grid-b">
          <div class="ui-block-a">
            <img src="assets/images/myIcons/video-camera.png" class="iconleft"/> 
          </div>
          <div class="ui-block-b">
            About us Video <em> ( Video ) </em>
          </div>
          <div class="ui-block-c">
            <!--  <a href="" data-icon="home" data-iconpos="notext" data-direction="reverse">Home</a> -->
          </div>
        </div>
      </a>


      <p id="testToSpeech2">Select to read about us, watch some slides or enjoy a screen cast?</p>
      <!--audio element-->
      <p>
        <input type="image" class="audioBtn" id="verify2" data-theme="a" alt="image for audio button" />
        <div id="audioholder2" style="height:0;"></div>
      </p>

      <!--basic pop up-->
      <div class="popUpdiv">
        <a href="#popupBasic" data-role="button" data-rel="popup" data-inline="true">Options</a>
      </div>

      <div data-role="popup" id="popupBasic">
        <p>Select to read about us, watch some slides or enjoy a screen cast?<p>
        </div>
        <!--end of pop up-->

      </div><!-- /content -->

      <div class="ui-grid-b">
        <div class="ui-block-a"><a href="#" data-rel="back"><button type="button" data-theme="a" data-icon="arrow-l">Back</button></a></div>
        <div class="ui-block-b"><a href="#instructions"><button type="button" data-theme="a" data-icon="arrow-l">Help</button></a></div>
        <div class="ui-block-c"><a href="home"><button type="button" data-theme="a">Home</button></a></div>     
      </div><!-- /grid-b -->

    </div><!-- /page one -->



    <!--**************************** About menu pop ups **********************************************-->


    <!-- Start of #FandQ popup -->
    <div data-role="page" id="FandQ">

      <div data-role="header" data-theme="b">
        <h1>wordUnravel F & Q</h1>
      </div><!-- /header -->

      <div role="main" class="ui-content">
        <h1 class="maintitle centreText">F and Q</h1>

        <div class="ui-body ui-body-a ui-corner-all">

          <div data-role="collapsible" data-inset="false" data-theme="c" data-content-theme="d">
            <h4>What is Wordunravel?</h4>
            <p>Word unravel is a platform for a puzzle game called...Word Unravel!. If you sign up you
              can play the game, create your own content to play, and have your own platform page.</p>
            </div>

            <div data-role="collapsible" data-inset="false" data-theme="c" data-content-theme="d">
              <h4>How do I create my own folders?</h4>
              <p>Sign up to WordUnravel, go to your platform page and select the "create folder" option.</p>
            </div>

            <div data-role="collapsible" data-inset="false" data-theme="c" data-content-theme="d">
              <h4>Can i use any words in my folders?</h4>
              <p>Yes. WordUnravel encourages users to create their own content, so you can get better at spelling the words you want to learn.
                Your words are spell-checked and are only submit after a spell check. Please note that WordUnravel administation
                check for any unsuitable words, so please do not add inappropriate words.</p>
              </div>

              <div data-role="collapsible" data-inset="false" data-theme="c" data-content-theme="d">
                <h4>Do I have to create word folders to play?</h4>
                <p>No. You can select the "public folders"option on your platform page and play word folders other users
                  have submitted.</p>
                </div>

                <div data-role="collapsible" data-inset="false" data-theme="c" data-content-theme="d">
                  <h4>Will everyone see my word folders?</h4>
                  <p>When you create a folder you have an option to keep your folder private, or make it public. If you make
                    your folder public other users have a chance to play your word folders. Then you can view how other people score on 
                    your folders!</p>
                  </div>

                  <div data-role="collapsible" data-inset="false" data-theme="c" data-content-theme="d">
                    <h4>What is my graph summary?</h4>
                    <p>Each time you play a folder, Word Unravel notes your results and creates a graph of your progress.
                      So you can watch your progress with a folder and see which words are troubling you.</p>
                    </div>

                    <div data-role="collapsible" data-inset="false" data-theme="c" data-content-theme="d">
                      <h4>Can I edit my folder anytime?</h4>
                      <p>Yes you can edit your folder anytime you want, change the images, change the words, change the difficulty settings and
                        change its status, making it public or private.</p>
                      </div>

                    </div>
                    <!--need to debug this code to work with top pop up rel button. at teh moemnt one stops the otehr working-->
                    <!--  <div><a class="top" data-ajax="false" data-role="button" href="#FandQ">Back to Top</a> </div> -->

                  </div><!-- /content -->

                  <div data-role="footer">

                    <p><a href="help" data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Back to Help</a></p>
                  </div><!-- /footer -->
                </div><!-- end of FandQ popup-->

                <!--**************************** text pop up **********************************************-->
                <div data-role="page" id="text_Popup" class="aboutText">

                  <div data-role="header" data-theme="b">
                    <h1>WordUnravel</h1>
                  </div><!-- /header -->
                  <!--  <div role="main" class="ui-content"> -->
                  <div id="aboutTextPopup">
                    <h1 class="maintitle centreText">Explaining WordUnravel</h1>
                    <p>
                    Word Unravel is your platform for the fun word puzzle game you will become addicted to. 
                    Create your own word folders, add images to words, and play the puzzle game with your folders
                    to help improve your memory and spelling.</p>
                    <p>Create folders with words you have trouble with 
                    and in no time you will get great at spelling them.</p>
                    <p>Make your folders public so others can play your folders and see if they can get a 
                    better score than you. Play other peoples folders and improve your spelling all round!
                    Your scores are stored and presented in a graph form so that you can watch your 
                    personal progress and see what words are troubling you.</p>
                    <p>Be in charge of your own learning, create your own content, decide on difficulty levels 
                    and comment on other peoples folding. All in your own time, on the move or at home on the sofa. 
                    Either way you won't want be able to stop playing the game. 
                    </p>

                  </div>


</div><!--end of page-->

<!--**************************** video pop up **********************************************-->
<div data-role="page" id="text_Video" class="aboutText">

  <div data-role="header" data-theme="b">
    <h1>wordUnravel</h1>
  </div><!-- /header -->

  <div id="videoPopup">
    <video id="movie" width="100%" height="310" preload controls>
      <source src=" assets/media/aboutMoviev3v1.m4v" />

      <source src="pr6.webm" type="video/webm; codecs=vp8,vorbis" />
      <source src="pr6.ogv" type="video/ogg; codecs=theora,vorbis" />
      <source src="pr6.mp4" />
      <object width="320" height="240" type="application/x-shockwave-flash"
      data="flowplayer-3.2.1.swf">
      <param name="movie" value="flowplayer-3.2.1.swf" />
      <param name="allowfullscreen" value="true" />
      <param name="flashvars" value="config={'clip': {'url': 'http://wearehugh.com/dih5/pr6.mp4', 'autoPlay':false, 'autoBuffering':true}}" />
      <p>Download video as <a href="pr6.mp4">MP4</a>, <a href="pr6.webm">WebM</a>, or <a href="pr6.ogv">Ogg</a>.</p>
    </object>
  </video>
  <script>
    var v = document.getElementById("movie");
    v.onclick = function() {
      if (v.paused) {
        v.play();
      } else {
        v.pause();
      }
    };
  </script>

</div>



</div><!--end of page-->


<!--********** PAGE ****************** objectives_menu **********************************************-->


<!-- Start of #about_menu -->
<div data-role="page" id="objectives_menu" data-theme="a">

  <div role="main" class="ui-content" >

    <a data-role="button" data-theme="a" href="#objectivesText_Popup" data-rel="dialog" data-transition="pop" data-icon="arrow-r" data-iconpos="right" class="aboutBtn">
      <div class="ui-grid-b">
        <div class="ui-block-a">
          <img src="assets/images/myIcons/eye.png" class="iconleft"/> 
        </div>
        <div class="ui-block-b">
          Our Objectives <em> ( Text ) </em>
        </div>
        <div class="ui-block-c">
        </div>
      </div>
    </a>
    <a data-role="button" data-theme="a" href="objectiveSlides" rel="external" data-transition="slide" data-icon="arrow-r" data-iconpos="right" class="aboutBtn">
      <div class="ui-grid-b">
        <div class="ui-block-a">
          <img src="assets/images/myIcons/picture-o.png" class="iconleft"/> 
        </div>
        <div class="ui-block-b">
          Our Objectives <em> ( Slides ) </em>
        </div>
        <div class="ui-block-c">
        </div>
      </div>
    </a>

    <a data-role="button" data-transition="pop" a href="#objectives_Video" data-rel="dialog"  data-icon="arrow-r" data-iconpos="right" class="aboutBtn">
      <div class="ui-grid-b">
        <div class="ui-block-a">
          <img src="assets/images/myIcons/video-camera.png" class="iconleft"/> 
        </div>
        <div class="ui-block-b">
          Our Objectives <em> ( Video ) </em>
        </div>
        <div class="ui-block-c">
        </div>
      </div>
    </a>


    <a data-role="button" data-transition="pop" a href="Resources" data-rel="dialog"  data-icon="arrow-r" data-iconpos="right" class="aboutBtn">
      <div class="ui-grid-b">
        <div class="ui-block-a">
          <img src="assets/images/myIcons/wrench.png" class="iconleft"/> 
        </div>
        <div class="ui-block-b">
          Resources for learning styles
        </div>
        <div class="ui-block-c">
        </div>
      </div>
    </a>

    <p id="testToSpeech3">Select to read about our objectives and goals, or see our slides, watch a video abot our objectives, or check out our resources pahe to help with your learning style</p>
    <!--audio element-->
    <p>
    <input type="image" class="audioBtn" id="verify3" data-theme="a" lt="image for audio button" />
      <div id="audioholder3" style="height:0;"></div>
    </p>
    <!--basic pop up-->
    <div class="popUpdiv">
      <a href="#popupObjectives" data-role="button" data-rel="popup" data-inline="true">Options</a>
    </div>

    <div data-role="popup" id="popupObjectives">
      <p>Select to read about our objectives and goals (Text), watch our slides, or view a screen cast about our objectives(video). Check out our resources page to help with your learning style.</p>
      <!--audio element--><p>
    </div>
    <!--end of pop up-->


    <div class="ui-grid-b">
      <div class="ui-block-a"><a href="#" data-rel="back"><button type="button" data-theme="a" data-icon="arrow-l">Back</button></a></div>
      <div class="ui-block-b"><a href="#instructions"><button type="button" data-theme="a" data-icon="arrow-l">Help</button></a></div>
      <div class="ui-block-c"><a href="home"><button type="button" data-theme="a">Home</button></a></div>     
    </div><!-- /grid-b -->


  </div><!-- /content -->



  <footer data-role="footer">
    <div data-role="navbar" data-iconpos="left">
      <ul id="footerText">
        <li><a href="#" data-icon="bookmark">Bookmark</a></li>
        <li><a href="#" data-icon="facebook" class="ui-btn-active">Facebook</a></li>
        <li><a href="platform" data-icon="user" data-ajax = "false">Platform</a></li>
        <li><a href="contactus" data-icon="envelope" data-transition="slide">Contact</a></li>

      </ul>

    </div><!-- /navbar -->
  </footer><!-- /footer -->
</div><!-- /page one -->



<!--**************************** objectives pop ups **********************************************-->

<!--**************************** ojjectives text pop up **********************************************-->
<div data-role="page" id="objectivesText_Popup" class="aboutText">

  <div data-role="header" data-theme="b">
    <h1>Objectives</h1>
  </div><!-- /header -->
  <div id="aboutTextPopup">
    <h1 class="maintitle">Val model learning styles Objectives</h1>
    <p>Visual, Auditory and Kinesthetic (VAK) learning style model</p>

    <p>A common and widely-used model of learning style is Fleming’s (2001) Visual Auditory Kinesthetic (VAK) model. According to this model, most people possess a dominant or preferred learning style; however some people have a mixed and evenly balanced blend of the three styles:</p>
    <ol>
      <li> Visual learners</li>
      <li>Auditory learners</li>
      <li> Kinaesthetic learners</li>
    </ol>

    <p class="heading">Visual learners tend to:
      <ul>
        <li>Learn through seeing</li>
        <li>Think in pictures and need to create vivid mental images to retain information</li>
        <li>Enjoy looking at maps, charts, pictures, videos, and movies</li>
        <li>Have visual skills which are demonstrated in puzzle building, reading, writing, understanding charts and graphs, a good sense of direction, sketching, painting, creating visual metaphors and analogies (perhaps through the visual arts), manipulating images, constructing, fixing, designing practical objects, and interpreting visual images</li>
      </ul>
    </p>
     
    <p class="heading">Auditory learners tend to:</p>
    <ul>
      <li> Learn through listening</li>
      <li> Have highly developed auditory skills and are generally good at speaking and presenting</li>
      <li>hink in words rather than pictures</li>
      <li>Learn best through verbal lectures, discussions, talking things through and listening to what others have to say</li>
      <li>Have auditory skills demonstrated in listening, speaking, writing, storytelling, explaining, teaching, using humour, understanding the syntax and meaning of words, remembering information, arguing their point of view, and analysing language usage</li>
    </ul>

    <p class="heading">Kinaesthetic learners tend to:</p>
    <ul>
      <li> Learn through moving, doing and touching</li>
      <li> Express themselves through movement</li>
      <li>Have good sense of balance and eye-hand coordination</li>
      <li>Remember and process information through interacting with the space around them</li>
      <li> Find it hard to sit still for long periods and may become distracted by their need for activity and exploration</li>
      <li> Have skills demonstrated in physical coordination, athletic ability, hands on experimentation, using body language, crafts, acting, miming, using their hands to create or build, dancing, and expressing emotions through the body.</li>
    </ul>  

    <p class="heading">Word Unravel</p>
    <p>Word Unravel implements elements which integrate different learning styles</p>
    <p>User fills in a Learning Style Questionnaire form on signing up. The form provides personal learning style results, information about particular learning style, and recommendations for using the game. E.g. Turn images on.</p>
    <p class="heading">VAK implementations in platform</p>
    <ul>
      <p class="heading">Kinesthetic: </p>
      <ul>
        <li>Slides</li>
        <li>Touch screen user-face</li>
        <li>Interaction with content (user can re-arrange their platform page).</li> 
        <li>Short blocks of information. </li>
      </ul>

      <p class="heading">Auditory: </p>
      <ul>
        <li>Auditory options (text-to-speech) in help sections and applicable pages.</li>
        <li>Videos in help section.</li>
      </ul>

      <p class="heading">Visual: </p>
      <ul>
        <li>Colorful layout </li>
        <li>Visual icons </li>
        <li>Visual representations (videos, slides, Organizational mind maps). </li>
        <li>Colors to highlight important points. </li>
        <li>Platform theme personalization</li>
        <li>Charts, graphs, diagrams. </li>
        <li>Reading. </li>
        <li>Demonstrations. </li>
      </ul>





    </p>

  </div>


</div><!--end of page-->

<!--**************************** objectives video pop up **********************************************-->
<div data-role="page" id="objectives_Video" class="aboutText">

  <div data-role="header" data-theme="b">
    <h1>wordUnravel</h1>
  </div><!-- /header -->

  <h1 class="maintitle centreText">Our objectives</h1>
  <div id="videoPopup">

    <video id="objective_movie" width="100%" height="310" preload controls>


    <source src=" assets/media/objectivesMoviev1.m4v" />
      <source src="pr6.webm" type="video/webm; codecs=vp8,vorbis" />
      <source src="pr6.ogv" type="video/ogg; codecs=theora,vorbis" />
      <source src="pr6.mp4" />
      <object width="320" height="240" type="application/x-shockwave-flash"
      data="flowplayer-3.2.1.swf">
      <param name="movie" value="flowplayer-3.2.1.swf" />
      <param name="allowfullscreen" value="true" />
      <!--     <param name="flashvars" value="config={'clip': {'url': 'http://wearehugh.com/dih5/pr6.mp4', 'autoPlay':false, 'autoBuffering':true}}" /> -->
      <param name="flashvars" value="config={'clip': {'url': 'assets/media/test.m4v', 'autoPlay':false, 'autoBuffering':true}}" />
      <p>Download video as <a href="pr6.mp4">MP4</a>, <a href="pr6.webm">WebM</a>, or <a href="pr6.ogv">Ogg</a>.</p>
    </object>
  </video>
  <script>
    var v = document.getElementById("objective_movie");
    v.onclick = function() {
      if (v.paused) {
        v.play();
      } else {
        v.pause();
      }
    };
  </script>

</div>

</div><!--end of page-->











