<div data-role="page" id="about_menu" data-theme="a">
 <div class="ui-body ui-body-a ui-corner-all">

    <div role="main" class="ui-content" >
        <a href="login" data-ajax="false" data-role = "button" data-icon="lock" data-iconpos="top">Log in</a>
        <a href="sign_up" data-ajax="false" data-icon="sign-in" data-role = "button" data-iconpos="top">Sign up</a>
    </div><!-- /content -->

</div>

    <footer data-role="footer">
        <div data-role="navbar">
            <ul>
                <li><a href="#" data-icon="plus">bookmark</a></li>
                <li><a href="#" data-icon="star" class="ui-btn-active">Facebook</a></li>
                <li><a href="#" data-icon="gear">share</a></li>
                <li><a href="#" data-icon="gear">contact</a></li>
            </ul>
        </div><!-- /navbar -->
    </footer><!-- /footer -->

</div><!-- /page one -->

