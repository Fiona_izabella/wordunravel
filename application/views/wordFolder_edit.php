<script type='text/javascript'>
$Spelling.SpellCheckAsYouType('all')
function validateFormSpelling(){
if($Spelling.BinSpellCheckFields('all')){
	alert("Spell Check OK - Submit The Form.")	
	return true
}else{
	alert("Spell Check Errors - Spelling Will Be Checked Before Submitting The Form.")
	$Spelling.SubmitFormById  = 'word_form';
	$Spelling.SpellCheckInWindow('all') 	;	
	return false;
	}
}
</script>

<div class ="page_wrapper">

<div class="heading">

<!--debugging code-->
<?php //print_r($this->session->userdata); ?>
 <?php  //echo "welcome $currentUser";?>
<!-- Are you logged in?: <?php //var_dump($this->session->userdata('logged_in')); ?> -->
<!-- <p class="inline">folder id: <?php //echo empty($folder->id) ? '' : $folder->id; ?></p> -->
<!--end of debugging code-->

<?php $currentUser = $this->session->userdata('username');?>
<?php $currentUserId =  $this->session->userdata('id');?>
<h1 class="maintitle inline"><?php  echo "welcome $currentUser";?></h1>

<p id="testToSpeech">Create new folders and editing existing folders. Fill in the words and select whether
the folder should be private or public. Decide on a difficulty rating which everyone can see if your 
folder is public. Add image from a database to create menumonics for your words. Have fun.</p>

      <input type="image" class="audioBtn-inline" id="verify4" data-theme="a" />
      <div id="audioholder" style="height:0;"></div>
    
<div class="clear"></div>
</div>


<div class="page_container"> 
<h2><?php echo empty($folder->id) ? 'Add a new folder' : 'Edit folder ' . '<span class="emphasize">'.$folder->name.'</span>'; ?></h2>
<?php echo form_open('', array('id' => 'word_form', 'onsubmit'=> 'return validateFormSpelling();')); ?>
<ul>
	<li>Folder Status</li>
	<li>
	<select name="folderRole">
	<option value="private" <?php echo set_select('folderRoles', 'private', ($folder->folderRole == 'private')); ?> >private</option>
	<option value="public" <?php echo set_select('folderRoles', 'public', ($folder->folderRole == 'public')); ?> >public</option>
	</select> 
	</li>



<?php $difficultyRating = $folder->difficulty; ?>	

<input type="hidden" name="folderOwner" id="folderOwner" value="<?php echo $currentUserId;?>" />
<li>Current difficulty level: <?php echo $difficultyRating; ?></li>
<li><label class="ui-mobile label emphasize">Change difficulty level (1 - 5)</label></li>
<li>
<input type="radio" name="difficulty" value="1" <?php echo set_radio('difficulty', 1); ?>/>
<input type="radio" name="difficulty" value="2" <?php echo set_radio('difficulty', 2); ?> />
<input type="radio" name="difficulty" value="3" <?php echo set_radio('difficulty', 3); ?> />
<input type="radio" name="difficulty" value="4" <?php echo set_radio('difficulty', 4); ?> />
<input type="radio" name="difficulty" value="5" <?php echo set_radio('difficulty', 5); ?> />
</li>
<li id = "note">Enter a difficulty rating. If folder is public other users can change this.</li>

<div class="clear"></div>
<li><label>Folder Name</label></li>
<?php $data = array(
'name'        => 'name',
'id'          => 'name',
'maxlength'   => '100'
//'required'       => 'required',
); ?>
<li><?php echo form_input($data, set_value('name', $folder->name)); ?><?php echo form_error('name', '<div class="errorForm">', '</div>'); ?>
<p id="bad_email"></p></li>

<hr class="carved" />

<li><label class="ui-mobile label emphasize">word one</label></li>
<?php $data = array(
'name'        => 'wordOne',
'id'          => 'wordOne',
'maxlength'   => '100',
//'required'       => 'required',
); ?>
<li><?php echo form_input($data, set_value('wordOne', $folder->wordOne)); ?><?php echo form_error('wordOne', '<div class="errorForm">', '</div>'); ?>
<p id="bad_email"></p></li>
<li class="myButton-a-inlineClear-addImage"><?php echo anchor('addImage', 'Add image to WordOne', array('data-ajax' => 'false'));?></li>

<hr class="carved" />

<li><label class="ui-mobile label emphasize">word two</label></li>
<?php $data = array(
'name'        => 'wordTwo',
'id'          => 'wordTwo',
'maxlength'   => '100',
'title'       => 'required',
); ?>
<li><?php echo form_input($data, set_value('wordTwo', $folder->wordTwo)); ?><?php echo form_error('wordTwo', '<div class="errorForm">', '</div>'); ?>
<p id="bad_email"></p></li>
<li class="myButton-a-inlineClear-addImage"><?php echo anchor('addImage', 'Add image to WordTwo', array('data-ajax' => 'false'));?></li>

<hr class="carved" />

<li><label class="ui-mobile label emphasize">word three</label></li>
<?php $data = array(
'name'        => 'wordThree',
'id'          => 'wordThree',
'maxlength'   => '100',
'title'       => 'required',
); ?>
<li><?php echo form_input($data, set_value('wordThree', $folder->wordThree)); ?><?php echo form_error('wordThree', '<div class="errorForm">', '</div>'); ?>
<p id="bad_email"></p></li>
<li class="myButton-a-inlineClear-addImage"><?php echo anchor('addImage', 'Add image to WordThree', array('data-ajax' => 'false'));?></li>

<hr class="carved" />

<li><label class="ui-mobile label emphasize">word four</label></li>
<?php $data = array(
'name'        => 'wordFour',
'id'          => 'wordFour',
'maxlength'   => '100',
'title'       => 'required',
); ?>

<li><?php echo form_input($data, set_value('wordFour', $folder->wordFour)); ?><?php echo form_error('wordFour', '<div class="errorForm">', '</div>'); ?>
<p id="bad_email"></p></li>
<li class="myButton-a-inlineClear-addImage"><?php echo anchor('addImage', 'Add image to WordFour', array('data-ajax' => 'false'));?></li>

<hr class="carved" />

<li><label class="ui-mobile label emphasize">word five</label></li>
<?php $data = array(
'name'        => 'wordFive',
'id'          => 'wordFive',
'maxlength'   => '100',
'title'       => 'required',
); ?>

<li><?php echo form_input($data, set_value('wordFive', $folder->wordFive)); ?><?php echo form_error('wordFive', '<div class="errorForm">', '</div>'); ?>
<p id="bad_email"></p></li>
<li class="myButton-a-inlineClear-addImage"><?php echo anchor('addImage', 'Add image to WordFive', array('data-ajax' => 'false'));?></li>

<hr class="carved" />

<li><label class="ui-mobile label emphasize">word six</label></li>
<?php $data = array(
'name'        => 'wordSix',
'id'          => 'wordSix',
'maxlength'   => '100',
'title'       => 'required',
); ?>

<li><?php echo form_input($data, set_value('wordSix', $folder->wordSix)); ?><?php echo form_error('wordSix', '<div class="errorForm">', '</div>'); ?>
<p id="bad_email"></p></li>
<li class="myButton-a-inlineClear-addImage centreText"><?php echo anchor('addImage', 'Add image to WordSix', array('data-ajax' => 'false'));?></li>

<hr class="carved" />

<li><?php echo form_submit('submit', 'Save', 'class="myButton-a extraMargin"'); ?></li>


<?php echo anchor('myFolders', '<li class="myButton-a-inlineClear-addImage extraMargin">Cancel</li>', array('data-ajax' => 'false')); ?>
</ul>

</form> 


</div><!--!page container-->

<!--bottom sub navigation-->
<ul>
<?php echo anchor('myFolders', '<li class="myButton-a-inline">Your folders</li>', array('data-ajax' => 'false')); ?>
<?php echo anchor('platform', '<li class="myButton-a-inline">Your Platform</li>', array('data-ajax' => 'false')); ?>
<?php echo anchor('yourplatform/logout', '<li class="myButton-a-inline">Log out</li>', array('data-ajax' => 'false')); ?>
</ul>
</div><!--!page_wrapper-->
</div>







