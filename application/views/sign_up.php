<div data-role="page" id="registerPage">

    <div data-role="header">
        <a href="index.html" data-rel="back">Home</a>
        <h1>Register Page</h1>
    </div>

    <div data-role="content">   
        <div class="ui-body ui-body-a ui-corner-all">

            <?php  if ($this->session->flashdata('validation_errors')){
                $validation_errors = $this->session->flashdata('validation_errors');
                echo '<table id="table_validation_errors">';
                echo '<tr><td>' . trim($validation_errors['errors']) . '</td></tr>';
                echo '</table>';
                echo '<br /><br />';
                $name = isset($validation_errors['post_data']['name']) ? $validation_errors['post_data']['name'] : null;
                $email = isset($validation_errors['post_data']['email']) ? $validation_errors['post_data']['email'] : null;
            }
            ?>

            <div id="errorBox"><ul></ul></div>

            <form action="sign_upValidation" method="post" id="registerForm" data-ajax ="false">

                <fieldset data-role="fieldcontain"> 
                    <label for="username">Username:</label>
                    <input type="text" name="username" id="username" class="required" minlength="5">
                    <!--need to make form error  work for is unique rule-->
                    <?php echo form_error('username', '<div class="errorForm">', '</div>'); ?>
                </fieldset>

                <fieldset data-role="fieldcontain"> 
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="password" class="required" minlength="5">
                </fieldset>

                <fieldset data-role="fieldcontain"> 
                    <label for="password2">Confirm Password:</label>
                    <input type="password" name="passconf" id="passconf" class="required passmatch" minlength="5">
                </fieldset>

                <fieldset data-role="fieldcontain"> 
                    <label for="email">Email:</label>
                    <input type="email" name="email" id="email" class="required email" minlength="5">
                </fieldset>

                <fieldset data-role="fieldcontain"> 
                    <label for="date">Date Input:</label>
                    <input type="date" name="date" id="date" value="" class="required" />  
                </fieldset>

                <input type="submit" data-theme="b" name="submit" id="submit" value="Submit">

            </form>
        </div>

    </div>

    <div data-role="footer">
        <h4>Footer content</h4>
    </div>

</div>


