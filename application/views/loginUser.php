<div data-role="page" id="one" data-theme="a"  data-url="DragPlatform4" >
	<div data-role="header">
		<h1 class="">Word Unravel</h1>
	</div>
	<div role="main" class="ui-content" >

		<div>
			<div class="ui-body ui-body-a ui-corner-all">

				<h1 class="maintitle centreText">Log in</h1>
				<p class="centreText">Please log in your with your email and password</p>

				<div>
					<!--print out flashdata error message if login fails-->
					<p class = "emailflashdata">
						<?php if ($this->session->flashdata('error') != ''): 
						echo $this->session->flashdata('error'); 
						endif; ?>
					</p>


					<form action="login" method="post" class = "login_form" data-ajax ="false", rel="external", name = 'login_form'>

						<table>
							<tr>
								<td>Username</td>
								<td><?php echo form_input('username'); ?><?php echo form_error('username', '<div class="logErrors">', '</div>'); ?></td>
							</tr>
							<tr>
								<td>Password</td>
								<td><?php echo form_password('password'); ?><?php echo form_error('password', '<div class="logErrors">', '</div>'); ?></td>
							</tr>
							<tr>
								<td></td>
								<td><?php echo form_submit('submit', 'Log in', 'class="btn-custom btn-primary btn-log" data-icon="lock"'); ?></td>
							</tr>
						</table>

					</form>

				</div>
			</div>

		</div>
	</div>