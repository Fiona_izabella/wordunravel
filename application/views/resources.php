<div data-role="page">

	<div data-role="header">
		<h1>WordUnravel Resources</h1>
	</div><!-- /header -->

	<div data-role="content">	
		<div class="content-primary">
			<div class="resources">
				<ul data-role="listview">
					<li data-role="list-divider">A</li>
					<li><a href="http://ehdimeeting.org/2012/Users/Uploads/pdfs/647SallyTannenbaum.pdf" rel ="external" target="blank">Auditory learning guide</a></li>
					<li><a href="http://www.jcu.edu.au/wiledpack/modules/fsl/JCU_090460.html" rel ="external" target="blank">All learning styles</a></li>
					<li><a href="http://www.whatismylearningstyle.com/auditory-learner.html" rel ="external" target="blank">Auditory learner</a></li>
					<li data-role="list-divider">H</li>
					<li><a href="http://www2.le.ac.uk/departments/gradschool/training/eresources/teaching/theories/honey-mumford" rel ="external" target="blank">Honey and Mumford</a></li>
					<li data-role="list-divider">K</li>
					<li><a href="http://www.facstaff.bucknell.edu/jvt002/Docs/ASEE-2008b.pdf" rel ="external" target="blank">Kinesthetic Learning in the Classroom</a></li>
					<li><a href="http://www.primary-education-oasis.com/kinesthetic-learning-style.html" rel ="external" target="blank">Kinesthetic Learning Style</a></li>
					<li><a href="http://www.businessballs.com/kolblearningstyles.htm" rel ="external" target="blank">Kolb learning styles</a></li>
					<li data-role="list-divider">L</li>
					<li><a href="http://www.eazhull.org.uk/initiatives/KingswoodFS/learning_styles.htm" rel ="external" target="blank">Learning styles</a></li>
					<li data-role="list-divider">V</li>
					<li><a href="http://psychology.about.com/od/educationalpsychology/a/vark-learning-styles.htm" rel ="external" target="blank">VARK Learning Styles</a></li>
					<li><a href="http://school.familyeducation.com/intelligence/teaching-methods/38519.html" rel ="external" target="blank">Visual, Auditory, Kinesthetic Learners</a></li>
					<li><a href="http://www.studygs.net/visual.htm" rel ="external" target="blank">Visual / spatial learning</a></li>


				</ul>
			</div><!--resources-->
		</div><!--/content-primary -->		
	</div><!-- /content -->

	<div class="ui-grid-b">
		<div class="ui-block-a"><a href="#" data-rel="back"><button type="button" data-theme="a" data-icon="arrow-l">Back</button></div>
		<div class="ui-block-b"><a href="help"><button type="button" data-theme="a" data-icon="arrow-l">Help menu</button></a></div>
		<div class="ui-block-c"><a href="home"><button type="button" data-theme="a">Home</button></a></div>     
	</div><!-- /grid-b -->

	<div data-role="footer">
		<h4>Word Unravel</h4>
</div><!-- /footer -->