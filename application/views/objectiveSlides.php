<div data-role="page" id="page1" class="slidesPage"> 
	<div data-role="content">
	 <a data-icon="home" data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Back</a>
	
		<div class="slideView"><img src="assets/images/slideContent/slide1.jpg" alt="slide 1 explaining our objectives"></div>
			<ul data-role="listview" data-inset="true" data-theme="a">
				<li id="listitem" data-icon="arrow-r" data-iconpos="right"><a href="#" data-transition="slide">NEXT SLIDE.. swipe left....</a></li>
				
			</ul>	
	</div>

</div>

<div data-role="page" id="page2" class="slidesPage"> 
	<div data-role="content">
	 <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
<div class="slideView"><img src="assets/images/slideContent/slide2.png" alt="slide 2 explaining our objectives"></div>
		<ul data-role="listview" data-inset="true" data-theme="a">
		
			<li id="listitem2" data-icon="arrow-r" data-iconpos="right"><a href="#">Slide 2 .... NEXT SLIDE...swipe the bar left</a></li> 
			<li id="listitemb2" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE .... swipe right</a></li>
		
		
		</ul>
	</div>

</div>

<div data-role="page" id="page3" class="slidesPage"> 
	<div data-role="content">
	 <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
	<div class="slideView"><img src="assets/images/slideContent/slide3.png" alt="slide 3 explaining our objectives"></div>
		<ul data-role="listview" data-inset="true" data-theme="a">
		<li id="listitem3" data-icon="arrow-r" data-iconpos="right"><a href="#">Slide 3 .... NEXT SLIDE...swipe the bar left</a></li> 
			<li id="listitemb3" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE .... swipe right</a></li>
		</ul>
	
	</div>

</div>

<div data-role="page" id="page4" class="slidesPage"> 
	<div data-role="content">
	 <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
      <div class="slideView"><img src="assets/images/slideContent/slide4.png" alt="slide 4 explaining our objectives"></div>
		<ul data-role="listview" data-inset="true" data-theme="a">
			<li id="listitem4" data-icon="arrow-r" data-iconpos="right"><a href="#">Slide 4 .... NEXT SLIDE...swipe the bar left</a></li> 
			<li id="listitemb4" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE .... swipe right</a></li>
		</ul>
		
	</div>

</div>

<div data-role="page" id="page5" class="slidesPage"> 
	<div data-role="content">
	 <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
         <div class="slideView"><img src="assets/images/slideContent/slide5.png"></div>
		<ul data-role="listview" data-inset="true" data-theme="a">
		<li id="listitem5" data-icon="arrow-r" data-iconpos="right"><a href="#" alt="slide 5 explaining our objectives">Slide 5 .... NEXT SLIDE...swipe the bar left</a></li> 
			<li id="listitemb5" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE .... swipe right</a></li>
		</ul>
		
	</div>

</div>

<div data-role="page" id="page6" class="slidesPage"> 
	<div data-role="content">
	 <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
     <div class="slideView"><img src="assets/images/slideContent/slide6.png" alt="slide 6 explaining our objectives"></div>
		<ul data-role="listview" data-inset="true" data-theme="">
			<li id="listitem6" data-icon="arrow-r" data-iconpos="right"><a href="#">Slide 6 .... NEXT SLIDE...swipe the bar left</a></li> 
			<li id="listitemb6" data-icon="arrow-l" data-iconpos="left"><a href="#">pPREVIOUS SLIDE .... swipe right</a></li>
		</ul>
		
	</div>

</div>

<div data-role="page" id="page7" class="slidesPage"> 
	<div data-role="content">
	 <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
      <div class="slideView"><img src="assets/images/slideContent/slide7.png" alt="slide 7 explaining our objectives"></div>
		<ul data-role="listview" data-inset="true" data-theme="a">
			<li id="listitem7" data-icon="arrow-r" data-iconpos="right"><a href="#">Slide 7 .... NEXT SLIDE...swipe the bar left</a></li> 
			<li id="listitemb7" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE .... swipe right</a></li>
		</ul>
		
	</div>

</div>

<div data-role="page" id="page8" class="slidesPage"> 
	<div data-role="content">
	 <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
        <div class="slideView"><img src="assets/images/slideContent/slide8.png" alt="slide 8 explaining our objectives"></div>
		<ul data-role="listview" data-inset="true" data-theme="a">
			<li id="listitem8" data-icon="arrow-r" data-iconpos="right"><a href="#">Slide 8 .... NEXT SLIDE...swipe the bar left</a></li> 
			<li id="listitemb8" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE .... swipe right</a></li>
		</ul>
		
	</div>

</div>

<div data-role="page" id="page9" class="slidesPage"> 
	<div data-role="content">
	 <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
<div class="slideView"><img src="assets/images/slideContent/slide9.png" alt="slide 9 explaining our objectives"></div>
		<ul data-role="listview" data-inset="true" data-theme="a">
			<li id="listitem9" data-icon="arrow-l" data-iconpos="right"><a href="#">Slide 9 .... NEXT SLIDE...swipe the bar left</a></li> 
			<li id="listitemb9" data-icon="arrow-r" data-iconpos="left"><a href="#">PREVIOUS SLIDE .... swipe right</a></li>
		</ul>
		
	</div>

</div>

<div data-role="page" id="page10" class="slidesPage"> 
	<div data-role="content">
	 <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
       <div class="slideView"><img src="assets/images/slideContent/slide10.png" alt="slide 10 explaining our objectives"></div>
		<ul data-role="listview" data-inset="true" data-theme="a">
		<li id="listitem10" data-icon="arrow-l" data-iconpos="right"><a href="#">Slide 10 .... NEXT SLIDE...swipe the bar left</a></li> 
			<li id="listitemb10" data-icon="arrow-r" data-iconpos="left"><a href="#">PREVIOUS SLIDE .... swipe right</a></li>
		</ul>
		
	</div>

</div>

<div data-role="page" id="page11" class="slidesPage"> 
	<div data-role="content">
	 <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
<div class="slideView"><img src="assets/images/slideContent/slide11.png" alt="slide 11 explaining our objectives"></div>
		<ul data-role="listview" data-inset="true" data-theme="a">
		<li id="listitem11" data-icon="arrow-l" data-iconpos="right"><a href="#">Slide 11 .... NEXT SLIDE...swipe the bar left</a></li> 
			<li id="listitemb11" data-icon="arrow-r" data-iconpos="left"><a href="#">PREVIOUS SLIDE .... swipe right</a></li>
		</ul>
		
	</div>

</div>

<div data-role="page" id="page12" class="slidesPage"> 
	<div data-role="content">
	 <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
<div class="slideView"><img src="assets/images/slideContent/slide12.png" alt="slide 12 explaining our objectives"></div>
		<ul data-role="listview" data-inset="true" data-theme="a">
	<li id="listitem12" data-icon="arrow-r" data-iconpos="right"><a href="#">Slide 12 .... NEXT SLIDE...swipe the bar left</a></li> 
			<li id="listitemb12" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE .... swipe right</a></li>
		</ul>
		
	</div>

</div>

<div data-role="page" id="page13" class="slidesPage"> 
	<div data-role="content">
	 <a data-icon="home" da href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
<div class="slideView"><img src="assets/images/slideContent/slide13.png" alt="slide 13 explaining our objectives"></div>
		<ul data-role="listview" data-inset="true" data-theme="a">
			<li id="listitem13" data-icon="arrow-r" data-iconpos="right"><a href="#">Slide 13</a></li> 
			<li id="listitemb13" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE .... swipe right</a></li>
		</ul>
		
	</div>

</div>

