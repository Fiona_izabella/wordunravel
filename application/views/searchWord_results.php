<div data-role="page" data-theme="a">

    <div data-role="header">
        <a href="" data-rel="back">Back</a>
        <h1>Word Results</h1>

        <a href="home" data-theme="b">Home</a>
    </div>


    <div data-role="content">    
        <h2>List of results</h2>
           <h5>Page under Construction</h5>
        <ul data-role="listview" data-inset="true">
            <li><a href="#">result link</a></li>
            <li><a href="#">result link</a></li>
            <li><a href="#">result link</a></li>
        </ul>
    </div>

    <footer data-role="footer">
    <div data-role="navbar" data-iconpos="left">
      <ul id="footerText">
            <li><a href="#" data-icon="bookmark">Bookmark</a></li>
            <li><a href="#" data-icon="facebook" class="ui-btn-active">Facebook</a></li>
            <li><a href="platform" data-icon="user" data-ajax = "false">Platform</a></li>
            <li><a href="contactus" data-icon="envelope" data-transition="slide">Contact</a></li>

      </ul>

    </div><!-- /navbar -->
  </footer><!-- /footer -->

</div>