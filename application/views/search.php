<div data-role="page" data-theme="a">

    <div data-role="header">
        <h1>Search</h1>
    </div>

    <div data-role="content">   

        <div data-inline="true">
            <h1 class="maintitle">Search site</h1>
            <form action="searchWord_results" method="post">
                <input type="search" name="search" data-inline="true"> <input type="submit" value="Search site" data-inline="true">
            </form>
        </div>

        <div data-inline="true">
            <h1 class="maintitle">Search words</h1>
            <form action="searchSite_results" method="post">
                <input type="search" name="search" data-inline="true"> <input type="submit" value="Search words" data-inline="true">
            </form>
        </div>
    </div>

    <footer data-role="footer">
    <div data-role="navbar" data-iconpos="left">
      <ul id="footerText">
            <li><a href="#" data-icon="bookmark">Bookmark</a></li>
            <li><a href="#" data-icon="facebook" class="ui-btn-active">Facebook</a></li>
            <li><a href="platform" data-icon="user" data-ajax = "false">Platform</a></li>
            <li><a href="contactus" data-icon="envelope" data-transition="slide">Contact</a></li>

      </ul>

    </div><!-- /navbar -->
  </footer><!-- /footer -->

</div>