<div class ="page_wrapper">

<div class="heading">
<h1 class="maintitle centreText"><?php echo empty($profile->id) ? 'Add a new Profile' : 'Edit Profile ' . $profile->username; ?></h1>
</div>

<div class="page_container"> 

<h3>profile id: <?php echo empty($profile->id) ? '' : $profile->id; ?></h3>

<form action="" method="post" id="registerForm" data-ajax ="false">

<l1><label class="ui-mobile">Username</label></l1>
<?php $data = array(
'name' => 'username',
'id'          => 'username',
'maxlength'   => '10',
'required'       => 'required'
); ?>
<li><?php echo form_input($data, set_value('username', $profile->username)); ?><?php echo form_error('username', '<div class="errorForm">', '</div>'); ?>
<p id="bad_email"></p></li>

<l1><label class="ui-mobile label">Email</label></l1>
<?php $data = array(
'name'        => 'email',
'id'          => 'email',
'maxlength'   => '100',
'class' => 'ui-shadow-inset ',
'required'       => 'required'
); ?>
<li><?php echo form_input($data, set_value('email', $profile->email)); ?><?php echo form_error('email', '<div class="errorForm">', '</div>'); ?>
<p id="bad_email"></p></li>

<?php $time = strtotime($profile->Bdate); ?>
<?php $bday = date('Y/m/d', $time); ?>

<l1><label class="ui-mobile label">Birthday: <?php echo "$bday"; ?></label></l1>

<l1><label class="ui-mobile label">Edit birthday</label></l1>
<input type="text" placeholder="Birthday Date" id="datepicker"  name="Bdate" class="formfield_text" value="<?php echo "$bday"; ?>">
<li class="ui-corner-all"><?php echo form_error('Bdate', '<div class="errorForm">', '</div>'); ?>
<p id="bad_email"></p></li>

<li><?php echo form_submit('submit', 'Save', 'class="myButton-a"'); ?></li>

</form><!--end of form div-->



<!--bottom buttons-->
<div><?php echo anchor('myProfile', 'My profile', 'class = "myButton-a-inline"');?></div>
<div><?php echo anchor('platform', 'Platform', 'class = "myButton-a-inline"');?></div>
<div><?php echo anchor('yourplatform/logout', 'Log out', 'class = "myButton-a-inline"');?></div>

</div>
</div><!--!page container-->

</div><!--!page_wrapper-->

   <script type="text/javascript">
    $(function() {
    	//need to format the date for the datPicker to be able to save to the database
       $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
    });
    </script>


  


