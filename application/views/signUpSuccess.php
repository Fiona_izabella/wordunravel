<div data-role="page" data-theme="a">

  <div data-role="header">
    <h1>You have registered!</h1>
  </div><!-- /header -->

  <div data-role="content">	

    <div role="main" class="ui-content" >
      <h2 class="centreText">Congratulations you have registered!</h2>
      <h2 class="centreText">Now you can log in to access your platform</h2>
      <a href="login" class="ui-btn ui-icon-delete ui-btn-icon-top">Log in</a>

      <h2>Take our Vak test to find out your learning style!</h2>
      <a href="#vakFormPopup" data-rel="popup" data-transition="pop" data-role="button" data-icon="delete" data-iconpos="left" data-mini="true" data-inline="true">What is this test?</a>
      <h2>You can choose to re-take the test anytime!</h2>

       <!--vak test simple pop-up-->
      <div data-role="popup" id="vakFormPopup">
        <div role="main" class="ui-content">
          <a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>
          Our Vak test is a set of question which helps determine your learning style.
          Once you know your learning style you can use the knowledge to make decisions which will
          speed up and help your learning and studies!
        </div><!-- /content -->
      </div>
    </div><!-- main -->



    <footer data-role="footer">
      <div data-role="navbar" data-iconpos="left">
        <ul id="footerText">
          <li><a href="#" data-icon="bookmark">Bookmark</a></li>
          <li><a href="#" data-icon="facebook" class="ui-btn-active">Facebook</a></li>
          <li><a href="#" data-icon="plus">Share</a></li>
          <li><a href="contactus" data-icon="envelope" data-transition="slide">Contact</a></li>

        </ul>

      </div><!-- /navbar -->
    </footer><!-- /footer -->
  </div><!-- content-->
</div><!--page -->




