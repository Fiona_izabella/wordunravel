<script type="text/javascript">
    function resetTextFields()
    {
        $("#postTitle").val("");
        $("#postContent").val("");
    }

    function onSuccess(data, status)
    {
        resetTextFields();
            // Notify the user the new post was saved
            $("#notification").fadeIn(2000);
            data = $.trim(data);
            if(data == "SUCCESS")
            {
                $("#notification").css("background-color", "#fff");
                $("#notification").text("The post was saved");
            }
            else
            {
                $("#notification").css("background-color", "#fff");
                $("#notification").text(data);
            }
            $("#notification").fadeOut(1000);
            }

$(document).ready(function() {
    $("#submit").click(function(){

        var formData = $("#newPostForm").serialize();

        $.ajax({
            type: "POST",              
            url:"<?php echo site_url('Blog/addPost');?>",
            cache: false,
            data: formData,
            success: onSuccess
        });

        return false;
    });


    $("#cancel").click(function(){
        resetTextFields();
    });

    $("#refresh").click(function(){
        location.reload();
    });
});

function redirect() {
    window.location.replace("blog");
    return false;
}

</script>

<!-- indexPage -->
<div data-role="page" id="blogHome">
    <div data-role="header">
        <h1>Welcome to blog comments!</h1>
    </div>
    <div data-role="content">
        <ul data-role="listview">
            <li>
                <a href="#createNewPostPage">Create New Comment</a>
            </li>
            <li>
                <a href="#readBlogPage">Read Comments</a>
            </li>
        </ul>
    </div>

 <div class="ui-grid-b">
            <div class="ui-block-a"><a href="#" data-rel="back"><button type="button" data-theme="a" data-icon="arrow-l">Back</button></a></div>
            <div class="ui-block-b"><a href="platform" data-ajax="false"><button type="button" data-theme="a" data-icon="arrow-l">Your Platform</button></a></div>
            <div class="ui-block-c"><a href="home" data-ajax="false"><button type="button" data-theme="a">Home</button></a></div>     
        </div><!-- /grid-b -->
         <div data-role="footer">
        <h1>WordUnravel Comments</h1>
    
    </div>
   


</div><!--page-->


<!-- createNewPostPage -->
<div data-role="page" id="createNewPostPage">

    <div data-role="header">
        <h1>Create New Comment</h1>
    </div>

   
        <div data-role="content">
          <form id="newPostForm" onsubmit="window.location.href = 'newlocation'; return false;"> 
                <div data-role="fieldcontain">
                    <label for="postTitle"><strong>Title:</strong></label>
                    <input type="text" name="postTitle" id="postTitle" value=""  />
                
                    <label for="postContent"><strong>Comment:</strong></label>
                    <textarea name="postContent" id="postContent"></textarea>
                     <div class="clear"></div>

                    </div>

                    <div class="ui-grid-a">
                        <div class="ui-block-a"><a href="#blogHome" id="cancel" data-role="button">Back to comments</a></div>
                        <div class="ui-block-b"><button data-theme="b" id="submit" type="submit">Submit</button></div>
                    </div>
                    <h3 id="notification"></h3>
                </div>
            </form>
      
        </div><!--content-->
  

        <div class="ui-grid-b">
            <div class="ui-block-a"><a href="#" data-rel="back"><button type="button" data-theme="a" data-icon="arrow-l">Back</button></a></div>
            <div class="ui-block-b"><a href="platform" data-ajax="false"><button type="button" data-theme="a" data-icon="arrow-l">Your Platform</button></a></div>
            <div class="ui-block-c"><a href="home" data-ajax="false"><button type="button" data-theme="a">Home</button></a></div>     
        </div><!-- /grid-b -->


  
</div><!--page-->




<!-- readBlogPage -->
<div data-role="page" id="readBlogPage">
    <div data-role="header">
        <h1>Comments</h1>
    </div>
    <div data-role="content">
    <div class="ui-grid-a">
            <div class="ui-block-a"><a href="#"><button type="button" data-iconpos="left" id="refresh" data-theme="b" data-icon="refresh">Refresh</button></a></div>
            <div class="ui-block-b"><a href="#createNewPostPage"><button type="button" data-theme="a" data-icon="arrow-l">Add comment</button></a></div>     
        </div><!-- /grid-a -->
      
        <ul data-role="listview" data-theme="d" data-inset="true">
            <?php
            try
            {
                $connection = mysql_connect("localhost","root","root");
                mysql_select_db("WordUnravel", $connection);
                $result = mysql_query("SELECT * FROM posts ORDER BY post_date desc");

                while($row = mysql_fetch_array($result))
                {
                    echo "<li><h2>" . $row['post_title'] . "</h2>" . $row['post_content'] . "<p class='ui-li-aside'>" . $row['post_date'] . "<strong></p>";
                }

                mysql_close($connection);
            }
            catch(Exception $e)
            {
                echo $e->getMessage();
            }
            ?>
        </ul>
        <div class="ui-grid-b">
            <div class="ui-block-a"><a href="#" data-rel="back"><button type="button" data-theme="a" data-icon="arrow-l">Back</button></a></div>
            <div class="ui-block-b"><a href="platform" data-ajax="false"><button type="button" data-theme="a" data-icon="arrow-l">Your Platform</button></a></div>
            <div class="ui-block-c"><a href="home" data-ajax="false"><button type="button" data-theme="a">Home</button></a></div>     
        </div><!-- /grid-b -->
    </div>

    <div data-role="footer">
        <h1>WordUnravel Comments</h1>
    </div>
</div><!--page-->

