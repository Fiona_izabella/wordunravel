<div data-role="page" id="page1" class="slidesPage"> 
  <div data-role="content">
     <a data-icon="home" data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Back</a>
    <div class="slideView"><img src="assets/images/vakImages/aboutSlide1.png" alt="slide one"></div>
      <ul data-role="listview" data-inset="true" data-theme="a">
        <li id="listitem" data-icon="arrow-r" data-iconpos="right"><a href="#" data-transition="slide" title="link for next slide">NEXT SLIDE.. swipe left....</a></li>
        
      </ul>
   
  </div>

</div>

<div data-role="page" id="page2" class="slidesPage"> 
  <div data-role="content">
   <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>

<div class="slideView"><img src="assets/images/vakImages/aboutSlide2.png" alt="slide two"></div>
    <ul data-role="listview" data-inset="true" data-theme="a">
    
      <li id="listitem2" data-icon="arrow-r" data-iconpos="right"><a href="#" title="link for next slide">Slide 2 .... NEXT SLIDE...swipe the bar left</a></li> 
      <li id="listitemb2" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE .... swipe the bar right</a></li>
    
    
    </ul>
  </div>

</div>

<div data-role="page" id="page3" class="slidesPage"> 
  <div data-role="content">
   <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
 <div class="slideView"><img src="assets/images/vakImages/aboutSlide2a.png" alt="slide three"></div>
    <ul data-role="listview" data-inset="true" data-theme="a">
    <li id="listitem3" data-icon="arrow-r" data-iconpos="right"><a href="#" title="link for next slide">Slide 3 .... NEXT SLIDE...swipe the bar left</a></li> 
      <li id="listitemb3" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE ....  swipe the bar right</a></li>
    </ul>
  
  </div>

</div>

<div data-role="page" id="page4" class="slidesPage"> 
  <div data-role="content">
   <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
  
      <div class="slideView"><img src="assets/images/vakImages/aboutSlide3.png" alt="slide four"></div>
    <ul data-role="listview" data-inset="true" data-theme="a">
      <li id="listitem4" data-icon="arrow-r" data-iconpos="right"><a href="#" title="link for next slide">Slide 4 .... NEXT SLIDE...swipe the bar left</a></li> 
      <li id="listitemb4" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE ....  swipe the bar right</a></li>
    </ul>
    
  </div>

</div>

<div data-role="page" id="page5" class="slidesPage"> 
  <div data-role="content">
   <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
         <div class="slideView"><img src="assets/images/vakImages/aboutSlide4.png" alt="slide five"></div>
    <ul data-role="listview" data-inset="true" data-theme="a">
    <li id="listitem5" data-icon="arrow-r" data-iconpos="right"><a href="#" title="link for next slide">Slide 5 .... NEXT SLIDE...swipe the bar left</a></li> 
      <li id="listitemb5" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE ....  swipe the bar right</a></li>
    </ul>
    
  </div>

</div>

<div data-role="page" id="page6" class="slidesPage"> 
  <div data-role="content">
   <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
  <div class="slideView"><img src="assets/images/vakImages/aboutSlide5.png" alt="slide six"></div>
    <ul data-role="listview" data-inset="true" data-theme="">
   <li id="listitem6" data-icon="arrow-r" data-iconpos="right"><a href="#" title="link for next slide">Slide 6 .... NEXT SLIDE...swipe the bar left</a></li>  
      <li id="listitemb6" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE ....  swipe the bar right</a></li>
    </ul>
    
  </div>

</div>

<div data-role="page" id="page7" class="slidesPage"> 
  <div data-role="content">
     <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
      <div class="slideView"><img src="assets/images/slideContent/gameslide1.png" alt="slide seven"></div>
    <ul data-role="listview" data-inset="true" data-theme="a">
      <li id="listitem7" data-icon="arrow-r" data-iconpos="right"><a href="#" title="link for next slide">Slide 7 .... NEXT SLIDE...swipe the bar left</a></li> 
      <li id="listitemb7" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE ....  swipe the bar right</a></li>
    </ul>
    
  </div>

</div> 

<div data-role="page" id="page8" class="slidesPage"> 
  <div data-role="content">
   <a data-icon="home" data-rel="back" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Back</a>
      <div class="slideView"><img src="assets/images/slideContent/gameslide2.png" alt="slide eight"></div>
    <ul data-role="listview" data-inset="true" data-theme="a">
      <li id="listitem8" data-icon="arrow-r" data-iconpos="right"><a href="#" title="link for next slide">Slide 8 .... NEXT SLIDE...swipe the bar left</a></li> 
      <li id="listitemb8" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE ....  swipe the bar right</a></li>
    </ul>
    
  </div>

</div> 

<div data-role="page" id="page9" class="slidesPage"> 
  <div data-role="content">
     <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
      <div class="slideView"><img src="assets/images/slideContent/gameslide3.png" alt="slide nine"></div>
    <ul data-role="listview" data-inset="true" data-theme="a">
      <li id="listitem9" data-icon="arrow-r" data-iconpos="right"><a href="#" title="link for next slide">Slide 9 .... NEXT SLIDE...swipe the bar left</a></li> 
      <li id="listitemb9" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE ....  swipe the bar right</a></li>
    </ul>
    
  </div>

</div> 

<div data-role="page" id="page10" class="slidesPage"> 
  <div data-role="content">
     <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
      <div class="slideView"><img src="assets/images/slideContent/gameslide4.png" alt="slide ten"></div>
    <ul data-role="listview" data-inset="true" data-theme="a">
      <li id="listitem10" data-icon="arrow-r" data-iconpos="right"><a href="#" title="link for next slide">Slide 10 .... NEXT SLIDE...swipe the bar left</a></li> 
      <li id="listitemb10" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE ....  swipe the bar right</a></li>
    </ul>
    
  </div>

</div> 

<div data-role="page" id="page11" class="slidesPage"> 
  <div data-role="content">
     <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
      <div class="slideView"><img src="assets/images/slideContent/gameslide5.png" alt="slide eleven"></div>
    <ul data-role="listview" data-inset="true" data-theme="a">
      <li id="listitem11" data-icon="arrow-r" data-iconpos="right"><a href="#" title="link for next slide">Slide 11 .... NEXT SLIDE...swipe the bar left</a></li> 
      <li id="listitemb11" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE ....  swipe the bar right</a></li>
    </ul>
    
  </div>

</div> 

<div data-role="page" id="page12" class="slidesPage"> 
  <div data-role="content">
     <a data-icon="home" a href="help" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Exit</a>
      <div class="slideView"><img src="assets/images/slideContent/gameslide6.png" alt="slide twelve"></div>
    <ul data-role="listview" data-inset="true" data-theme="a">
      <li id="listitemb12" data-icon="arrow-l" data-iconpos="left"><a href="#">PREVIOUS SLIDE ....  swipe the bar right</a></li>
    </ul>
    
  </div>

</div> 



<div class="ui-grid-b">
  <div class="ui-block-a"><a href="#" data-rel="back"><button type="button" data-theme="a" data-icon="arrow-l">Back</button></a></div>
  <div class="ui-block-b"><a href="help" data-ajax="false"><button type="button" data-theme="a" data-icon="arrow-l">Help</button></a></div>
  <div class="ui-block-c"><a href="home"><button type="button" data-theme="a">Home</button></a></div>     
</div><!-- /grid-b -->

</div><!--end of page-->