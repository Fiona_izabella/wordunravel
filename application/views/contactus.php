<div id="contactform" data-role="page" data-theme="a">
  <div data-role="header">
    <h1>Contact Form</h1>
    <a data-icon="home" data-rel="back">Back</a>
  </div>
  <div data-role="content" data-theme="a">	


    <!--echo out the flashdata 'message' if it is not blank-->
    <p class = "emailflashdata">
      <?php if ($this->session->flashdata('message') != ''): 
      echo '<script type="text/javascript">'
      , '</script>';
      echo $this->session->flashdata('message'); 
      endif; 
      ?>
    </p> 

    <?php  if ($this->session->flashdata('validation_errors')){
      $validation_errors = $this->session->flashdata('validation_errors');
      echo '<table id="table_validation_errors">';
      echo '<tr><td class="errorMessage">' . trim($validation_errors['errors']) . '</td></tr>';
      echo '</table>';
      echo '<br />';

      $name = isset($validation_errors['post_data']['name']) ? $validation_errors['post_data']['name'] : null;
      $email = isset($validation_errors['post_data']['email']) ? $validation_errors['post_data']['email'] : null;
    }
    ?>


    <!--these both work, and use if data-ajax doesnt work on "form(open)"-->
    <!--  <form action="http://localhost:8888/jqueryWordCI/contactus/sendcontactMail" method="post" accept-charset="utf-8" data-ajax="false" > -->
    <!--  <form action="/sendEmail" method="post" data-ajax="false" accept-charset="utf-8">  -->

    <!--add attributes to form in array-->
    <?php $attributes = array('class' => 'contact_form', 'name' => 'contact_form', 'data-ajax'=> 'false', 'data-url'=>'true'); ?>


    <?php echo form_open('contactus/sendcontactMail', $attributes); ?>
    <?php echo form_error();?>
    <ul class="contactForm">
      <li>
        <h2>Enter your details</h2>
        <span class="errorMessage">* All Fields Required</span>
      </li>
      <li>
        <?php echo form_label('name:', 'name');
        $data = array(
          'name' => 'name',
          'placeholder' => 'fiona',
          'required' => 'required',
          'value' => "$name"                            
          );
        echo form_error('name', '<div class="errorMessage">', '</div>'); 
        echo form_input($data);?>

      </li>
      <li>
        <?php echo form_label('Email:', 'email');
        $data = array(
          'name'        => 'email',
          'placeholder' => 'john_doe@example.com',
          'required' => 'required',
          'value' => "$email"
          );
          echo form_input($data);?>
          <?php echo form_error('email', '<div class="errorMessage">', '</div>'); ?> 

        </li>
        <li>
          <?php echo form_label('Subject:', 'subject');
          $data = array(
            'name'        => 'subject',
            'placeholder' => 'subject here',
            'cols' => '60',
            'rows' => '3',
            'required' => 'required'
            );
            echo form_textarea($data);?>
            <?php echo form_error('subject', '<div class="errorMessage">', '</div>'); ?> 

          </li>
          <li>
            <?php echo form_label('Message:', 'message');
            $data = array(
              'name'        => 'message',
              'placeholder' => 'query here',
              'cols' => '60',
              'rows' => '3',
              'required' => 'required'
              );
              echo form_textarea($data);?>
              <?php echo form_error('message', '<div class="errorMessage">', '</div>'); ?> 
            </li>

            <li>

              <?php $data = array(
                'name'  => 'submit',
                'value'  => 'send form',
                'class' => 'button'
                );
                echo form_submit($data); ?>
              </li>

              <li>

                <?php $data = array(
                  'name'  => 'reset',
                  'value'  => 'reset',
                  'class' => 'button'
                  );
                  echo form_reset($data); ?>
                </li>
              </ul>

              <?php echo form_close();?>

            </div>
            <div data-role="footer">

            </div>

