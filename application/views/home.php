<div data-role="page" id="one" data-theme="a">
  <div data-role="header">
    <h1 class="">Word Unravel</h1>
  </div>
  <div role="main" class="ui-content" >


    <div class="ui-body ui-body-a ui-corner-all">

      <p class="emphasize centreText">Welcome to WordUnravel</p> 
      <p class="centreAlign">Sign up and have your own platform for playing the best game around.</p>
      <p class="centreAlign"> Create your own content for the game
        and watch your progress as you improve. This addictive game will improve your memory, spelling, and allows you to take control of your learning in a fun
        and flexible way!</p>
        <p id="testToSpeech">Welcome to WordUnravel. Sign up and have your own platform for playing the best game around. Create your own content for playing
          and watch your progress as you improve. This addictive game will improve your memory,spelling, and allows you to take control of your learning in a fun
          and flexible way!</p>
          <!--audio element-->
          <p>
        <input type="image" class="audioBtn" id="verify" data-theme="a" alt="image button for audio sound" title="select for audio option" />
            <div id="audioholder" style="height:0;"></div> 
          </p>
        

        </div>
        <p><a href="help" data-role = "button" data-icon="arrow-r" data-iconpos="right" rel="external"  data-transition="slide" data-icon="arrow-r" data-iconpos="right">Want to know more?</a></p>
        <p><a href="platform" data-role ="button" data-theme="b" data-icon="arrow-r" data-iconpos="right" rel="external"  data-transition="slide" data-icon="arrow-r" data-iconpos="right">Your Platform</a></p>

      </div><!-- /content -->

      <footer data-role="footer">
        <div data-role="navbar" data-iconpos="left">
          <ul id="footerText">
            <li><a href="#" data-icon="bookmark">Bookmark</a></li>
            <li><a href="#" data-icon="facebook" class="ui-btn-active">Facebook</a></li>
            <li><a href="platform" data-icon="user" data-ajax = "false">Platform</a></li>
            <li><a href="contactus" data-icon="envelope" data-transition="slide">Contact</a></li>

          </ul>
 

        </div><!-- /navbar -->
      </footer><!-- /footer -->

    </div><!-- /page one -->

      
