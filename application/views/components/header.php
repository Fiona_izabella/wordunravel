<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Word Unravel</title>
  <meta name="viewport" content="height=device-height,width=device-width,initial-scale=1.0,maximum-scale=1.0" >
  <!---style sheets-->
  <link href="<?php echo site_url('assets/js/jquery-ui.css'); ?>" rel="stylesheet">
  <link href="<?php echo site_url('assets/css/wordUnravelTemes.css'); ?>" rel="stylesheet">
  <link href="<?php echo site_url('assets/css/jquery.mobile.icons.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo site_url('assets/css/jquery.mobile.structure-1.4.0.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo site_url('assets/css/mystyles.css'); ?>" rel="stylesheet">
  <link href="<?php echo site_url('assets/css/jqm-icon-pack-fa.css'); ?>" rel="stylesheet">

  <!--js-->
  <script src="<?php echo site_url('assets/js/jquery-1.11.0.min.js');?>"></script>
  <script src="<?php echo site_url('assets/js/jquery.com:ui:1.10.4:jquery-ui.js');?>"></script> 
  <script src="<?php echo site_url('assets/js/jquery.mobile-1.4.1.min.js');?>"></script> 
  <script src="assets/js/jquery.validate.js"></script>
  <script src="assets/js/jquery.ui.touch-punch.min.js"></script>
  <script src="assets/js/clockScript.js"></script>
  <script src="assets/js/jquery.udraggable.js"></script>

  <script type="text/javascript">
    <!--/*$(document).bind("mobileinit", function(){ $.mobile.ajaxLinksEnabled(false); });*/-->
    $(function(){
      $( "[data-role='header']" ).toolbar();
    });
  </script>

</head>
<body>
