<div data-role="header" data-position="fixed" data-theme="a" role="banner" class="ui-header ui-header-fixed slidedown ui-bar-a">

	<div data-role="navbar" data-grid="d" >
		<ul id="main_nav">

			<a href="#" id= "bk_btn" data-rel="back" data-role="button" data-icon="carat-l" data-iconpos="left"></a>
			<li><a href="home" data-role="button" id="logo" data-theme="a" data-iconpos="left" data-icon="myapp-custom"  alt="homepage" title="homepage"></a></li>
			<!--use session to determine whether user is logged in and change to a log out or log in button-->
			<?php if($this->session->userdata('logged_in')): ?>				
				<li><a href="yourplatform/logout" data-ajax="false" id="loginButton" data-role="button" data-theme="a" data-iconshadow="false" data-iconpos="left" data-icon="myapp-custom-logout" alt="log out" title="log out"></a></li>
			<?php else: ?>
				<li><a href="login_menu" id="loginButton" data-role="button" data-theme="a" data-iconshadow="false" data-iconpos="left" data-icon="myapp-custom-login" alt="log in" title="log in"></a></li>
			<?php endif;?>
			<li><a href="search" id="searchButton" data-role="button" data-iconshadow="false" class="ui-icon-nodisc" data-shadow="false" data-theme="a" data-iconpos="left" data-icon="myapp-custom-search"  alt="search" title="search"></a>
			</li>
			<li><a href="help" data-ajax ="false" id="helpButton" class="ui-btn-active ui-state-persist" data-role="button" data-theme="b" data-iconpos="left" data-icon="myapp-custom-help-up"  alt="help menu" title="help menu"></a>
			</li>
			
			<!--for debugging-->
			<!-- <l1>you are logged in?<?php //var_dump ($this->session->userdata('logged_in')); ?></li> -->
		</ul>
	</div><!-- /navbar -->
</div><!-- /header -->