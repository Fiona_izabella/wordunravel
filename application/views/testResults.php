<div data-role="page" id="home" date-theme="a">

  <div data-role="header">
    <h1 class="maintitle">Vak test for <?php echo $usersName ; ?></h1>
  </div><!-- /header -->

  <div data-role="content">	
    <div role="main" class="ui-body ui-body-a ui-corner-all" >

      <!--debugging code-->
      <?php //print_r($OverallResult); ?>
      <?php //print_r($combinedArray); ?>
      <?php //echo $usersId ; ?>
      <?php //echo "visual results: $totalCountOfVisual"  ;?>
      <?php //echo "audio results: $totalCountOfAudio"  ;?>
      <?php //echo "Kinesthetic results: $totalCountOfKinetic"  ;?>
      <?php //echo "top score: $topScore"  ;?>
      <?php //print_r($test); ?>
      <?php //print_r($kinestheticResults); ?>
      <?php //echo "this works?: $FunctionResultTest"  ;?>
      <!--end of debugging code-->

      <h1>Your results</h1>
      <div class="results conclusions">
        <h2>Your results</h2>
        <ul>

          <li><?php echo "visual results: $totalCountOfVisual"  ;?></li>
          <li><?php echo "audio results: $totalCountOfAudio"  ;?></li>
          <li><?php echo "Kinesthetic results: $totalCountOfKinetic"  ;?></li>
          <li>Top score: <?php echo $topScore ;?></li>
          <li>AS A LEARNER I AM... <span class="emphasizeLarge"><?php echo $FunctionResultTest  ;?></span></li>
        </div>

        <table>
          <tr>

            <td width="153"><strong>visual</strong></td>
            <td width="18"></td>
            <td width="163"><strong>Auditory</strong></td>
            <td width="18"></td>
            <td width="143"><strong>kinesthetic</strong></td>
          </tr>
          <tr>


            <td>learning by observing </td>
            <td></td>
            <td>learning by listening </td>
            <td></td>
            <td>learning by doing </td>


          </tr>
        </table>
        <!--add link depending on learning result-->
        <?php if ($FunctionResultTest == "auditory") :?>
          <?php echo anchor('audioLearning', 'What does this mean?', array('data-ajax' => 'false', 'data-role' => "button", )); ?>
        <?php elseif ($FunctionResultTest == "visual"): ?>
          <?php echo anchor('visualLearning', 'What does this mean?', array('data-ajax' => 'false', 'data-role' => "button", )); ?>
        <?php elseif ($FunctionResultTest == "kinesthetic"): ?>
          <?php echo anchor('kinestheticLearning', 'What does this mean?', array('data-ajax' => 'false', 'data-role' => "button", )); ?>
        <?php endif; ?>


        <div data-role="footer">
          <h4>Footer content</h4>
        </div><!-- /footer -->
      </div>	<!--content-->
</div><!-- /page -->