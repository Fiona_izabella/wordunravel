<div class ="page_wrapper">

	<div class="heading">
		<h1 class="maintitle centreText">Edit password</h1>
	</div><!-- /header -->

	<!--debugging code-->
	<?php //echo "your actual password: $password->password ";?>
	<?php  //echo "this is the password $password" ;?>
	<!--  <p>current password = <?php //echo $this->session->userdata('password'); ?></p> -->
	<?php //print_r($profileDetails); ?>
	<?php //print_r($this->session->all_userdata()); ?>
	<?php //echo "my password $check->password" ;?>
	<?php //echo "$notification" ;?>
	<!--end of debugging code-->

	<div class="page_container"> 

		<h2><?php echo $this->session->userdata('username'); ?>'s Password</h2>

		<?php foreach ($profileDetails as $d): ?>

<?php $attributes = array('id' => 'myform');//add attributes to the form
echo form_open("editPassword/editPass",$attributes);?>
<!--create ul li which includes the original values and set up a separate error message for each form_input-->
<li>Current Password</li>
<li><?php echo form_input('password'); ?><?php echo form_error('password', '<div class="errorLarge">', '</div>'); ?></li>

<li>new password </li>
<li><?php echo form_input('new_password'); ?><?php echo form_error('new_password', '<div class="errorLarge">', '</div>'); ?></li>

<li></li>
<li><?php echo form_submit('submit', 'Save', 'class="myButton-a"'); ?></li>
<?php endforeach; ?>
<?php echo form_close();?>


<!--bottom buttons-->
<div><?php echo anchor('myProfile', 'My profile', 'class = "myButton-a-inline"');?></div>
<div><?php echo anchor('platform', 'Platform', 'class = "myButton-a-inline"');?></div>
<div><?php echo anchor('yourplatform/logout', 'Log out', 'class = "myButton-a-inline"');?></div>

</div><!--!page container-->
</div><!--!page_wrapper-->

