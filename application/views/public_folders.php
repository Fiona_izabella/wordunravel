<div class ="page_wrapper">

<div class="heading">
<?php $AuthorizedUser = $this->session->userdata('username');?>
<?php echo "Hello $AuthorizedUser"; ?><br>

<!--debugging code-->
<!-- Are you logged in?: <?php //var_dump($this->session->userdata('logged_in')); ?> -->
<?php //print_r($allowEdit)  ;?>
<?php //foreach($allowEdit as $a): ?>
<?php //echo $a->folderOwner?> 
<?php //endforeach; ?> 
<?php //print_r($folders) ?>
<!--end of debugging code-->
</div>


<div class="page_container"> 
	<h2>Public Folders</h2>
	<?php //echo anchor('wordFolder/edit', '<i class="icon-plus"></i> Add a folder','class=""', array('data-ajax' => 'false')); ?>
	<a href="wordFolder/edit" class ="addfolder" data-icon="delete" data-ajax="false">Add a folder</a>
	<table class="table table-stripe">
		<thead>
			<tr>
			    <th>Folder name</th>
				<th>Created</th>
				<th>Modified</th>
				<th>Folder Author</th>
				<th>Folder Status</th>
				<th>Edit</th>
				<th>Delete</th> 
				<!--learn<th><?php// echo $users[1] -> name ;?></th>this outputs the second in the array-->
			</tr>
		</thead>
		<tbody>
<?php if(count($folders)): foreach($folders as $f): ?>	
		<tr>	
		<!-- <a href="wordFolder/edit/" class ="addfolder" data-icon="delete" data-ajax="false">Add a folder</a> -->
		   <td><?php echo anchor('wordFolder/edit/' . $f->id, $f->name, array('data-ajax' => 'false')); ?></td>
		   <?php $timestamp = strtotime($f->created); ?>
		    <?php $modifiedDate = strtotime($f->modified); ?>
		   <td><?php echo date('d/m/Y', $timestamp); ?></td>
		   <td><?php echo date('d/m/Y', $modifiedDate); ?></td>
		   <td><?php echo $f->folderOwner; ?></td>
		    <td><?php echo $f->folderRole; ?></td>
		    <?php $folderOwner = $f->folderOwner;
		    if ($AuthorizedUser == $folderOwner): ?>
			<td><?php echo anchor('wordFolder/edit/' . $f->id, 'edit', array('data-ajax' => 'false')); ?></td> 
			<?php else: ?>
			<td>edit</td> 

		   <?php endif; ?> 
			<td><?php echo anchor('wordFolder/delete/' . $f->id, 'delete', array('data-ajax' => 'false')); ?></td>
		</tr>
<?php endforeach; ?>
<?php else: ?>
		<tr>
			<td colspan="3">Your folder is empty.</td>
		</tr>
<?php endif; ?>	
		</tbody>
	</table>
</div>
</div>





<div class=""><?php echo anchor('DragPlatform4', 'Cancel', 'class = "btn-custom"');?></div>
<div class="btn-custom"><?php echo anchor('platform', 'Your Platform');?></div>
<div class="btn-custom"><?php echo anchor('yourplatform/logout', 'Log out');?></div>



